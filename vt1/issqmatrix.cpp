#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include <sstream>
#include "issqmatrix.h"

TEST_CASE("Test I, format","[correct format]"){
	CHECK(isSquareMatrix("[[1]]"));
	CHECK(isSquareMatrix("[[1,-2][1,4]]"));
	CHECK(isSquareMatrix("[[1,2,3][1,4,-5][2,7,8]]"));	
    CHECK(isSquareMatrix("[[1,-2,3,4][1,4,5,6][2,7,8,9][7,3,-5,8]]"));
}

TEST_CASE("Test II, format","[incorrect format]"){
	CHECK_FALSE(isSquareMatrix("1]]"));
	CHECK_FALSE(isSquareMatrix("[1]]"));
	CHECK_FALSE(isSquareMatrix("[[1,2][1,4]"));
	CHECK_FALSE(isSquareMatrix("[[1,2,3]1,4,5][2,7,8]]"));
	CHECK_FALSE(isSquareMatrix("[[1,2,3][1,4,5[2,7,8]]"));	
    CHECK_FALSE(isSquareMatrix("[[1,2,3,4][1,4,5,6][2,7,8,9][7,3,5,8"));;
}

TEST_CASE("Test III, dimensions","[correct dimensions]"){
	CHECK(isSquareMatrix("[[1]]"));
	CHECK(isSquareMatrix("[[1,2][1,4]]"));
	CHECK(isSquareMatrix("[[1,2,3][1,4,5][2,7,8]]"));	
    CHECK(isSquareMatrix("[[1,2,3,4][1,4,5,6][2,7,8,9][7,3,5,8]]"));
}

TEST_CASE("Test IV, dimensions","[incorrect dimensions]"){
	CHECK_FALSE(isSquareMatrix("[[1][1,2]]"));
	CHECK_FALSE(isSquareMatrix("[[1,2][1]]"));
	CHECK_FALSE(isSquareMatrix("[[1,2,3][1,4,5][2,7,9][3,6,8]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3,4][1,4,5,6][2,7,8,9][7,3,5,8][7,3,5,8]]"));;
}

/*
int main() {
	
	// Esimerkki [[1,2,3][4,5,6][7,8,9]]
	
	if (isSquareMatrix("[[1,2,3,4][1,4,5,6][2,7,8,9][7,3,5,8]]")) {
		std::cout << "OK" << std::endl;
	}
	else {
		std::cout << "EI OK" << std::endl; 
	}
	
	return 0;
}
*/

bool isSquareMatrix(const std::string &str) {
	char c;	
	int number;
	int matrix_columns = 0;
	int matrix_rows = 0;
	int matrix_expected_size = 0;
	
	std::stringstream input(str);
	
	input >> c;
	
	// Examine string input to verify correct square-matrix format
	// Example: ->[<-[1,2,3][4,5,6]] can not be anything else than [	
	if (!input.good() || c != '[')
		return false;
	
	input >> c;
	
	// While char is not ] input-reference: [[1,2,3][4,5,6]->]<-	
	while (c != ']') {
		
		// Return false if char not [ input-reference: [->[<-1,2,3][4,5,6]]
		if (!input.good() || c != '[')
			return false;
		
		matrix_columns = 0;
		
		do {
			
			input >> number;
			
			// Return false if input not integer, [[->1<-, 2,3]
			if (!input.good())
				return false;
			
			matrix_columns++;
			
			input >> c;	
			
			// Return false if input not , OR ]
			if (!input.good() || (c != ',' && c != ']'))
				return false;
						
		} while (c != ']');		
		
		// Get square matrix dimensions
		if (matrix_rows == 0)
			matrix_expected_size = matrix_columns;
			
		matrix_rows++;
		
		// Check square-matrix colums & rows are equal
		if (matrix_expected_size != matrix_columns)
			return false;		
				
		input >> c;
		
		// Return false if input not [ or ]
		if (!input.good() || (c != '[' && c != ']'))
			return false;		
	}
	
	// Check square-matrix dimensions are correct
	if (matrix_expected_size != matrix_rows)
		return false;
	
	// Check input ends	
	std::string tail;
	input >> tail;
	return tail == "";
}
