/**
    \file squarematrix.cpp
    \brief SquareMatrix class code
*/
#include "squarematrix.h"
#include <ostream>
#include <sstream>

/* Squarematrix laskeminen, kertolasku
 * 
 * [ a11, a12 ] * [ b11, b12 ]
 * [ a21, a22 ]   [ b21, b22 ]
 * =
 * [ a11 * b11 + a12 * b21 , a11 * b12 + a12 * b22 ]
 * [ a21 * b11 + a22 * b21 , a21 * b12 + a22 * b22 ]
 *
 * [1,2] * [5,6]
 * [3,4]   [7,8]
 * 
 * [1*5 + 2*7 , 1*6 + 2*8]
 * [3*5 + 4*7 , 3*6 + 4*8]
 * =
 * [19 , 22]
 * [43 , 50]
*/

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& m) {	
	
	// [ e11 , e12 ]
	// [ e21 , e22 ]
	e11 += m.e11;
	e12 += m.e12;
	e21 += m.e21;
	e22 += m.e22;
	
	return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& m) {
	
	e11 -= m.e11;
	e12 -= m.e12;
	e21 -= m.e21;
	e22 -= m.e22;
	
	return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& m) {
	
	IntElement r11 = e11 * m.e11 + e12 * m.e21;
	IntElement r12 = e11 * m.e12 + e12 * m.e22;
	IntElement r21 = e21 * m.e11 + e22 * m.e21;
	IntElement r22 = e21 * m.e12 + e22 * m.e22;
	
	e11 = r11;
	e12 = r12;
	e21 = r21;
	e22 = r22;
	
	return *this;
}

// outputs the matrix into the stream os in the form [[1,2][3,4]]
void SquareMatrix::print(std::ostream& os) const {
	
	os << toString();
}

// returns string representation of the matrix in the form [[1,2][3,4]]
std::string SquareMatrix::toString() const {
	
	std::stringstream ss;
	
	ss << "[[";
	ss << e11 << "," << e12;
	ss << "][" << e21 << "," << e22;
	ss << "]]";
	
	return ss.str();
}
