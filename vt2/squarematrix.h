/**
    \file squarematrix.h
    \brief Header for SquareMatrix class
*/
#ifndef SQUAREMATRIX_H_INCLUDED
#define SQUAREMATRIX_H_INCLUDED
#include "intelement.h"
#include <string>

/**
    \class SquareMatrix
    \brief A class for square matrices
*/
class SquareMatrix {
	
	private:
		IntElement e11;
		IntElement e12;
		IntElement e21;
		IntElement e22;
		
	public:
		
		/**
        	\brief Parametric constructor
        	\param i11 - i22, IntElements forming the square matrix
    	*/
		SquareMatrix(
			const IntElement& i11,
			const IntElement& i12,
			const IntElement& i21,
			const IntElement& i22
			)
			:e11{i11}, e12{i12}, e21{i21}, e22{i22}{};
		
		/**
        	\brief Default constructor
    	*/
		SquareMatrix():SquareMatrix{0,0,0,0}{};
		
		/**
        	\brief Copy constructor
    	*/
		SquareMatrix(const SquareMatrix& m)
			:e11{m.e11}, e12{m.e12}, e21{m.e21}, e22{m.e22}{};
		
		/**
        	\brief Destructor
    	*/
		virtual ~SquareMatrix() = default;
		
		/**
        	\brief Operator for adding SquareMatrices
        	\param SquareMatrix to add
        	\return Addition result as SquareMatrix
    	*/
		SquareMatrix& operator +=(const SquareMatrix& m);
		
		/**
        	\brief Operator for subtracting SquareMatrices
        	\param SquareMatrix to subtract
        	\return Subtraction result as SquareMatrix
    	*/
		SquareMatrix& operator -=(const SquareMatrix& m);
		
		/**
        	\brief Operator for multiplying SquareMatrices
        	\param SquareMatrix to multiply
        	\return Multiplication result as SquareMatrix
    	*/
		SquareMatrix& operator *=(const SquareMatrix& m);
		
		/**
        	\brief Print SquareMatrix string representation to ostream
        	\param stream to print into
    	*/
		void print(std::ostream&) const;
		
		/**
        	\brief Creates a string presentation of the SquareMatrix 
        	\return string presentation of SquareMatrix: [[1,2][3,4]]
    	*/
		std::string toString() const;
};

#endif // SQUAREMATRIX_H_INCLUDED
