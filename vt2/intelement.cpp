/**
    \file intelement.cpp
    \brief IntElement class code
*/
#include "intelement.h"

int IntElement::getVal() const {
	return val;
}

void IntElement::setVal(int v) {
	val = v;
}

IntElement& IntElement::operator+=(const IntElement &i) {
	val += i.val;
	return *this;
}

IntElement& IntElement::operator-=(const IntElement &i) {
	val -= i.val;
	return *this;
}

IntElement& IntElement::operator*=(const IntElement &i) {
	val *= i.val;
	return *this;
}

// Basic operators
IntElement operator+(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum += j;
	return sum;
}

IntElement operator-(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum -= j;
	return sum;
}

IntElement operator*(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum *= j;
	return sum;
}

std::ostream& operator<<(std::ostream& os, const IntElement& i) {
	
	os << i.getVal();
	
	return os;
}

