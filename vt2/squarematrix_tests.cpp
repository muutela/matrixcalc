/**
    \file squarematrix_tests.cpp
    \brief Tests for SquareMatrix class
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "squarematrix.h"
#include <sstream>
#include <ostream>

TEST_CASE("SquareMatrix, calculations I","[true]"){
	
	SquareMatrix sm1{1,2,3,4};
	SquareMatrix sm2{5,6,7,8};
	
	sm1 += sm2;
	
	CHECK(sm1.toString() == "[[6,8][10,12]]");
	
	SquareMatrix sm3{1,2,3,4};
	SquareMatrix sm4{5,6,7,8};
	
	sm3 -= sm4;
	
	CHECK(sm3.toString() == "[[-4,-4][-4,-4]]");
	
	SquareMatrix sm5{1,2,3,4};
	SquareMatrix sm6{5,6,7,8};
	
	sm5 *= sm6;
	
	CHECK(sm5.toString() == "[[19,22][43,50]]");
}

TEST_CASE("SquareMatrix, calculations II","[false]"){
	
	SquareMatrix sm1{1,2,3,4};
	SquareMatrix sm2{5,6,7,8};
	
	sm1 += sm2;
	
	CHECK_FALSE(sm1.toString() == "[[5,8][10,12]]");
	
	SquareMatrix sm3{1,2,3,4};
	SquareMatrix sm4{5,6,7,8};
	
	sm3 -= sm4;
	
	CHECK_FALSE(sm3.toString() == "[[-4,4][-4,-4]]");
	
	SquareMatrix sm5{1,2,3,4};
	SquareMatrix sm6{5,6,7,8};
	
	sm5 *= sm6;
	
	CHECK_FALSE(sm5.toString() == "[[19,22][40,50]]");
}

TEST_CASE("SquareMatrix, print & toString","[true]"){
	
	SquareMatrix sm1{1,2,3,4};
	std::stringstream ss;
	sm1.print(ss);
	CHECK(ss.str() == "[[1,2][3,4]]");
	
	CHECK(sm1.toString() == "[[1,2][3,4]]");
}

