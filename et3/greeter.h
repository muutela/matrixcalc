#ifndef GREETER_H_INCLUDED
#define GREETER_H_INCLUDED
#include <string>

class Greeter {
	
	private:
		std::vector<std::string> greetings;
		
	public:
		Greeter():greetings{}{};
		Greeter(std::string g):greetings{g}{};				
		virtual ~Greeter()=default;			
		std::string sayHello() const;
		void addGreet(const std::string&);
		void clearGreets();
};

#endif // GREETER_H_INCLUDED
