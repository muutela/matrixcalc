#include "greeter.h"
#include <vector>
#include <sstream>
#include <string>

std::string Greeter::sayHello() const {
	
	std::stringstream ss;
	
	for (auto i : greetings) {
		ss << i << std::endl;
	}
	
	return ss.str();
}

void Greeter::addGreet(const std::string& g) {
	
	greetings.push_back(g);
}

void Greeter::clearGreets() {
	
	greetings.clear();
}
