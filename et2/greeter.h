#ifndef GREETER_H_INCLUDED
#define GREETER_H_INCLUDED
#include <string>

class Greeter {
	
	private:
		std::string greetings;
		
	public:
		Greeter(const std::string& str):greetings{str}{};
		Greeter():greetings{""}{};
		
		virtual ~Greeter()=default;
		
		std::string sayHello() const;	
};

#endif // GREETER_H_INCLUDED
