#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "greeter.h"

TEST_CASE("Greeter basic tests", "[finnish]"){
	
	Greeter greet("Terve");	
	
	CHECK(greet.sayHello() == "Terve");
	CHECK_FALSE(greet.sayHello() == "Morjens");
}
