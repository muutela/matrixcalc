/**
    \file compositeelement.h
    \brief Header for CompositeElement class
*/
#ifndef COMPOSITEELEMENT_H_INCLUDED
#define COMPOSITEELEMENT_H_INCLUDED

#include "element.h"

#include <memory>
#include <functional>
#include <string>

/**
    \class CompositeElement
    \brief A class for CompositeElement
*/
class CompositeElement : public Element {
	
	private:
		
		std::unique_ptr<Element> oprnd1;
		std::unique_ptr<Element> oprnd2;
		
		std::function<int(int,int)> op_fun;
		char op_ch;
		
	public:
		
		/**
        	\brief Parametric constructor
    	*/
		CompositeElement(const Element&, const Element&, const std::function<int(int,int)>&, char);
		
		/**
        	\brief Copy constructor
    	*/
		CompositeElement(const CompositeElement&);
		
		/**
        	\brief Destructor
    	*/
		virtual ~CompositeElement()=default;
		/**
        	\brief Assignment operator for CompositeElement
        	\param const CompositeElement
        	\return CompositeElement&
    	*/
		CompositeElement& operator=(const CompositeElement&);
				
		/**
        	\brief Clone CompositeElement, overrides Element.clone()
        	\return Pointer to new Element
    	*/
		virtual Element* clone() const override;
		
		/**
        	\brief Creates a string presentation of CompositeElement, overrides Element.toString()
        	\return string representation of CompositeElement
    	*/
		virtual std::string toString() const override;
		
		/**
        	\brief Retrieves int value from a map<char,int> char symbol, overrides Element.evaluate()
        	\param Valuation map<char, int>
        	\return int value mapped to char
    	*/
		virtual int evaluate(const Valuation&) const override;
};

#endif
