/**
    \file element.cpp
    \brief Element class/interface code
*/
#include "element.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const Element& e){
	
	os << e.toString();
	
	return os;
}

bool operator==(const Element& e1, const Element& e2) {
	
	return e1.toString() == e2.toString();
}
