/**
    \file varelement.cpp
    \brief VariableElement class code
*/
#include "varelement.h"
#include "valuation.h"

#include <sstream>

char VariableElement::getVal() const {
	return val;
}

void VariableElement::setVal(char v) {
	val = v;
}

Element* VariableElement::clone() const {
	
	return new VariableElement(val);
}

std::string VariableElement::toString() const {
	
	std::stringstream ss;	
	ss << getVal();
	
	return ss.str();
}

int VariableElement::evaluate(const Valuation& v) const {
	
	return v.at(val);
}

