/**
    \file concretematrix.h
    \brief Header for ConcreteSquareMatrix class
*/
#ifndef CONCRETEMATRIX_H_INCLUDED
#define CONCRETEMATRIX_H_INCLUDED

#include "intelement.h"

#include <string>
#include <vector>
#include <memory>

class SymbolicSquareMatrix;

/**
    \class ConcreteSquareMatrix
    \brief A class for square matrices
*/
class ConcreteSquareMatrix {
	
	private:		
		int n;
		std::vector<std::vector<std::unique_ptr<IntElement>> > elements{};
		
		friend SymbolicSquareMatrix;
		
	public:
		
		/**
        	\brief Default constructor
    	*/
		ConcreteSquareMatrix():n{0}{};
		
		/**
			\brief Parametric constructor
			\param valid string for initializing ConcreteSquareMatrix "[[1,2][3,4]]"
			\throws std::invalid_argument if string is invalid
		*/		
		ConcreteSquareMatrix(const std::string& s);		
	
		/**
        	\brief Copy constructor
    	*/
		ConcreteSquareMatrix(const ConcreteSquareMatrix&);
		
		/**
        	\brief Move constructor
    	*/
		ConcreteSquareMatrix(ConcreteSquareMatrix&&);
		
		/**
        	\brief Destructor
    	*/
		virtual ~ConcreteSquareMatrix() = default;
		
    	/**
        	\brief Transpose ConcreteSquareMatrix
        	\return Transposed ConcreteSquareMatrix
    	*/
    	ConcreteSquareMatrix transpose() const;
    	
    	/**
        	\brief Check if input string is valid ConcreteSquareMatrix
        	\param String to be checked
    	*/
    	void isSquareMatrix(const std::string &str);
    	
    	/**
        	\brief Assignment operator for ConcreteSquareMatrix
        	\param Matrix m to be assigned to object value
        	\return ConcreteSquareMatrix
    	*/
    	ConcreteSquareMatrix& operator=(const ConcreteSquareMatrix& m);
    	
    	/**
        	\brief Move assignment operator for ConcreteSquareMatrix
        	\param Matrix m to be assigned to object value
        	\return ConcreteSquareMatrix
    	*/
    	ConcreteSquareMatrix& operator=(ConcreteSquareMatrix&& m);
    	
		/**
        	\brief Operator for adding SquareMatrices
        	\param ConcreteSquareMatrix m to be added
        	\throws std::domain_error if matrices sizes do not match
        	\return Addition result as ConcreteSquareMatrix
    	*/
		ConcreteSquareMatrix& operator +=(const ConcreteSquareMatrix& m);
		
		/**
        	\brief Operator for subtracting SquareMatrices
        	\param ConcreteSquareMatrix m to subtract
        	\throws std::domain_error if matrices sizes do not match
        	\return Subtraction result as ConcreteSquareMatrix
    	*/
		ConcreteSquareMatrix& operator -=(const ConcreteSquareMatrix& m);
		
		/**
        	\brief Operator for multiplying SquareMatrices
        	\param ConcreteSquareMatrix m to multiply
        	\throws std::domain_error if matrices sizes do not match
        	\return Multiplication result as ConcreteSquareMatrix
    	*/
		ConcreteSquareMatrix& operator *=(const ConcreteSquareMatrix& m);
    	
    	/**
        	\brief Equals operator for ConcreteSquareMatrix
        	\param Matrix m to be compared to object value
        	\return boolean
    	*/
    	bool operator==(const ConcreteSquareMatrix& m) const;
    	
    	/**
        	\brief Print ConcreteSquareMatrix string representation to ostream
        	\param stream to print into
    	*/
		void print(std::ostream&) const;
		
		/**
        	\brief Creates a string presentation of the ConcreteSquareMatrix 
        	\return string presentation of ConcreteSquareMatrix: [[1,2][3,4]]
    	*/
		std::string toString() const;
};

/**
    \brief Operator for ConcreteSquareMatrix addition
    \param first member of ConcreteSquareMatrix
    \param second member of ConcreteSquareMatrix
    \return The product of SquareMatrices
*/
ConcreteSquareMatrix operator+(const ConcreteSquareMatrix&, const ConcreteSquareMatrix&);

/**
    \brief Operator for ConcreteSquareMatrix subtraction
    \param first member of ConcreteSquareMatrix
    \param second member of ConcreteSquareMatrix
    \return The product of SquareMatrices
*/
ConcreteSquareMatrix operator-(const ConcreteSquareMatrix&, const ConcreteSquareMatrix&);

/**
    \brief Operator for ConcreteSquareMatrix multiplication
    \param first member of ConcreteSquareMatrix
    \param second member of ConcreteSquareMatrix
    \return The product of SquareMatrices
*/
ConcreteSquareMatrix operator*(const ConcreteSquareMatrix&, const ConcreteSquareMatrix&);

/**
    \brief Output operator
    \param osstream to print in
    \param SymbolicSquareMatrix to print from
    \return ostream
*/
std::ostream& operator<<(std::ostream&, const ConcreteSquareMatrix&);

#endif // ConcreteSquareMatrix_H_INCLUDED
