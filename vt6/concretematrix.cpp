/**
    \file concretematrix.cpp
    \brief ConcreteSquareMatrix class code
*/
#include "concretematrix.h"

#include <ostream>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <string>
#include <numeric>

ConcreteSquareMatrix::ConcreteSquareMatrix(const std::string& s) {
	
	isSquareMatrix(s);
}

ConcreteSquareMatrix::ConcreteSquareMatrix(const ConcreteSquareMatrix& m):n(m.n){
	
	
	for (auto& row : m.elements) {
		std::vector<std::unique_ptr<IntElement>> newrow;
		for (auto& elem : row) {
			newrow.push_back(std::unique_ptr<IntElement>(new IntElement(*elem)));
		}
		elements.push_back(std::move(newrow));
	}	
}

ConcreteSquareMatrix::ConcreteSquareMatrix(ConcreteSquareMatrix&& m){
	
	n = std::move(m.n); 
	elements = std::move(m.elements);
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator+=(const ConcreteSquareMatrix& m) {	
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	auto m_row = m.elements.begin();
	
	for (auto& row : elements) {
		std::transform(row.begin(), row.end(),
			m_row->begin(), row.begin(),
			[] (
			const std::unique_ptr<IntElement> &p1, const std::unique_ptr<IntElement> &p2) {
			IntElement sum = *p1+*p2; return std::unique_ptr<IntElement>(new IntElement(sum));
			});
		m_row++;
	}
	
	return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator-=(const ConcreteSquareMatrix& m) {
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	auto m_row = m.elements.begin();
	
	for (auto& row : elements) {
		std::transform(row.begin(), row.end(),
			m_row->begin(), row.begin(),
			[] (
			const std::unique_ptr<IntElement> &p1, const std::unique_ptr<IntElement> &p2) {
			IntElement sum = *p1-*p2; return std::unique_ptr<IntElement>(new IntElement(sum));
			});
		m_row++;
	}
	
	return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator*=(const ConcreteSquareMatrix& m) {
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	ConcreteSquareMatrix rightMat = m.transpose();
	std::vector<std::vector<std::unique_ptr<IntElement>> > newElements{};
	
	for (auto& lrow : elements) {
		std::vector<std::unique_ptr<IntElement>> nextRow{};
		
		for(auto& row: rightMat.elements) {
			std::vector<IntElement> prodRow(n);
			std::transform( row.begin(), row.end(), lrow.begin(), prodRow.begin(),
				[] (const std::unique_ptr<IntElement>& p1, const std::unique_ptr<IntElement>& p2) {	
				return (*p1)*(*p2); });
			IntElement sum = std::accumulate(prodRow.begin(), prodRow.end(),
				IntElement{0}, std::plus<IntElement>());
			nextRow.push_back(std::unique_ptr<IntElement>(new IntElement(sum))); // kloonaa summa, unique_ptr
		}
		
		newElements.push_back(std::move(nextRow));
		
	}
	
	elements = std::move(newElements);
	
	return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator=(const ConcreteSquareMatrix& m){
	
	if (this == &m) return *this;
	
	/*	
	for (auto& row : m.elements) {
		std::vector<std::unique_ptr<IntElement>> newrow;
		for (auto& elem : row) {
			newrow.push_back(std::unique_ptr<IntElement>(elem->clone()));
		}
		elements.push_back(std::move(newrow));
	}
	*/
	n = m.n;
	ConcreteSquareMatrix cpy(m);
	std::swap(elements, cpy.elements);
	
	return *this;
}

ConcreteSquareMatrix& ConcreteSquareMatrix::operator=(ConcreteSquareMatrix&& m){
	
	if (this == &m) return *this;
	
	n = std::move(m.n);	
	elements = std::move(m.elements);
	
	return *this;
}

ConcreteSquareMatrix ConcreteSquareMatrix::transpose() const {

	ConcreteSquareMatrix sm;
	
	std::vector<std::vector<std::unique_ptr<IntElement>> > new_elts(n);	
	
	// Pystyriveiksi tulee matriisin vaakarivit
	// [ 
	//   [1,2,3]    [1,1,2] 
	//   [1,4,5] -> [2,4,7]
	//   [2,7,8]    [3,5,8]
	
	//   [1,2,3][1,4,5][2,7,8]
	// 	 [1,1,2][2,4,7][3,5,8]
	
	
	for (auto& row : elements) {
		int i = 0;
		for (auto& elem : row) {
			//new_elts[i].push_back(elem); taytyy tehda uusi alkio, clone
			new_elts[i].push_back(std::unique_ptr<IntElement>(new IntElement(*elem)));
			i++;
		}		
	}
	
	sm.elements = std::move(new_elts);
	sm.n = n;
	return sm;
}

ConcreteSquareMatrix operator+(const ConcreteSquareMatrix& m1, const ConcreteSquareMatrix& m2) {	
	
	ConcreteSquareMatrix sm(m1);
	sm += m2;
	
	return sm;
}

ConcreteSquareMatrix operator-(const ConcreteSquareMatrix& m1, const ConcreteSquareMatrix& m2) {
	
	ConcreteSquareMatrix sm(m1);
	sm -= m2;
	
	return sm;
}

ConcreteSquareMatrix operator*(const ConcreteSquareMatrix& m1, const ConcreteSquareMatrix& m2) {
	
	ConcreteSquareMatrix sm(m1);	
	sm *= m2;
	
	return sm;
}

bool ConcreteSquareMatrix::operator==(const ConcreteSquareMatrix& m) const {
	
	return toString() == m.toString();
}

std::ostream& operator<<(std::ostream& os, const ConcreteSquareMatrix& m) {
	
	os << m.toString();

	return os;
}

// outputs the matrix into the stream os in the form [[1,2][3,4]]
void ConcreteSquareMatrix::print(std::ostream& os) const {
	
	os << toString();
}

// returns string representation of the matrix in the form [[1,2][3,4]]
std::string ConcreteSquareMatrix::toString() const {
	
	std::stringstream ss;
	
	ss << "[";
	
	for(auto& row:elements) {
		ss << "[";
		
		bool firstRound = true;
		for(auto& elem:row) {
			if (!firstRound) {
				ss << ",";
			}
			ss << *elem;
			firstRound = false;
		}
		ss << "]";
	}
	ss << "]";
	
	return ss.str();
}

void ConcreteSquareMatrix::isSquareMatrix(const std::string &str) {
	char c;	
	int number;
	int matrix_columns = 0;
	int matrix_rows = 0;
	int matrix_expected_size = 0;
	
	std::stringstream input(str);
	
	input >> c;
	
	// Examine string input to verify correct square-matrix format
	// Example: ->[<-[1,2,3][4,5,6]] can not be anything else than [	
	if (!input.good() || c != '[')
		throw std::invalid_argument("String is not a square matrix.");
	
	input >> c;
	
	// While char is not ] input-reference: [[1,2,3][4,5,6]->]<-	
	while (c != ']') {
		
		// Return false if char not [ input-reference: [->[<-1,2,3][4,5,6]]
		if (!input.good() || c != '[')
			throw std::invalid_argument("String is not a square matrix.");
		
		std::vector<std::unique_ptr<IntElement>> row;	
		
		matrix_columns = 0;
		
		do {
			
			input >> number;
			
			// char symbol = stream.peek();
			
			// Return false if input not integer, [[->1<-, 2,3]
			if (!input.good())
				throw std::invalid_argument("String is not a square matrix.");
			
			//row.push_back(IntElement{number});
			row.push_back(std::unique_ptr<IntElement>(new IntElement(number)));
			matrix_columns++;
			
			input >> c;
			
			// Return false if input not , OR ]
			if (!input.good() || (c != ',' && c != ']'))
				throw std::invalid_argument("String is not a square matrix.");
						
		} while (c != ']');
		
		elements.push_back(std::move(row));
		
		// Get square matrix dimensions
		if (matrix_rows == 0)
			matrix_expected_size = matrix_columns;
			
		matrix_rows++;
		
		// Check square-matrix colums & rows are equal
		if (matrix_expected_size != matrix_columns)
			throw std::invalid_argument("String is not a square matrix.");
				
		input >> c;
		
		// Return false if input not [ or ]
		if (!input.good() || (c != '[' && c != ']'))
			throw std::invalid_argument("String is not a square matrix.");	
	}
	
	// Check square-matrix dimensions are correct
	if (matrix_expected_size != matrix_rows)
		throw std::invalid_argument("String is not a square matrix.");
	
	// Check input ends, getline to get whitespaces
	std::string tail;
	std::getline(input, tail);
	
	if (tail != "" || isspace(tail.front())) {
		throw std::invalid_argument("String is not a square matrix.");		
	}
	
	n = matrix_expected_size;
}


