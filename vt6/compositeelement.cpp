/**
    \file compositeelement.cpp
    \brief CompositeElement class code
*/
#include "compositeelement.h"
#include "element.h"

#include <memory>
#include <sstream>

/*
CompositeElement::CompositeElement(const Element& e1, const Element& e2, 
					const std::function<int(int,int)>& opfun, char opc):
						op_fun{opfun}, op_ch{opc} {
	
	oprnd1 = std::unique_ptr<Element>(e1.clone());
	oprnd2 = std::unique_ptr<Element>(e2.clone());
	
	op_fun = opfun;
}
*/
CompositeElement::CompositeElement(const Element& e1, const Element& e2,
					const std::function<int(int,int)>& opfun,
					char opc) {
	
	op_ch = opc;
	
	oprnd1 = std::unique_ptr<Element>(e1.clone());
	oprnd2 = std::unique_ptr<Element>(e2.clone());
	
	op_fun = opfun;
}


CompositeElement::CompositeElement(const CompositeElement& c) {
	
	op_ch = c.op_ch;
	
	oprnd1 = std::unique_ptr<Element>(c.oprnd1->clone());
	oprnd2 = std::unique_ptr<Element>(c.oprnd2->clone());
	
	op_fun = c.op_fun;
}

CompositeElement& CompositeElement::operator=(const CompositeElement& c) {
	
	if (this == &c) return *this;	
	
	CompositeElement cpy(c);
	std::swap(op_ch, cpy.op_ch);
	std::swap(oprnd1, cpy.oprnd1);
	std::swap(oprnd2, cpy.oprnd2);
	std::swap(op_fun, cpy.op_fun);
	
	return *this;
}		

Element* CompositeElement::clone() const{
	
	return new CompositeElement(*this);
}


std::string CompositeElement::toString() const{
	
	std::stringstream ss;	
	
	ss << "(" << *oprnd1 << op_ch << *oprnd2 << ")";
	
	return ss.str();
}


int CompositeElement::evaluate(const Valuation& v) const {
	
	int op1 = oprnd1->evaluate(v);
	int op2 = oprnd2->evaluate(v);
	
	return op_fun(op1, op2);
}
