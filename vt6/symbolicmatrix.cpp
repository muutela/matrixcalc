/**
    \file symbolicmatrix.cpp
    \brief SymbolicSquareMatrix class code
*/
#include "symbolicmatrix.h"
#include "concretematrix.h"
#include "varelement.h"
#include "element.h"
#include "compositeelement.h"

#include <string>
#include <sstream>
#include <vector>
#include <memory>
#include <stdexcept>
#include <algorithm>
#include <numeric>

SymbolicSquareMatrix::SymbolicSquareMatrix(const std::string& s) {
	
	isSquareMatrix(s);
}

SymbolicSquareMatrix::SymbolicSquareMatrix(const SymbolicSquareMatrix& m):n(m.n){
	
	
	for (auto& row : m.elements) {
		std::vector<std::unique_ptr<Element>> newrow;
		for (auto& elem : row) {
			newrow.push_back(std::unique_ptr<Element>(elem->clone()));
		}
		elements.push_back(std::move(newrow));
	}	
}

SymbolicSquareMatrix::SymbolicSquareMatrix(SymbolicSquareMatrix&& m){
	
	n = std::move(m.n); 
	elements = std::move(m.elements);
}
		
SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(const SymbolicSquareMatrix& m) {
	
	n = m.n;
	SymbolicSquareMatrix cpy(m);
	std::swap(elements, cpy.elements);
	
	return *this;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(SymbolicSquareMatrix&& m){
	
	if (this == &m) return *this;
	
	n = std::move(m.n);	
	elements = std::move(m.elements);
	
	return *this;
}
		
SymbolicSquareMatrix SymbolicSquareMatrix::transpose() const {

	SymbolicSquareMatrix sm;
	
	std::vector<std::vector<std::unique_ptr<Element>> > new_elts(n);		
	
	for (auto& row : elements) {
		int i = 0;
		for (auto& elem : row) {
			new_elts[i].push_back(std::unique_ptr<Element>(elem->clone()));
			i++;
		}		
	}
	
	sm.elements = std::move(new_elts);
	sm.n = n;
	return sm;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator-(const SymbolicSquareMatrix& m) {
	
	if (n!=m.n) {
		throw std::domain_error("Matrix dimensions do not match");		
	}
	
	SymbolicSquareMatrix ssm;
	ssm.n = n;
	
	auto mrow = m.elements.begin();
	for (auto& row : elements) {
		auto melem = mrow->begin();
		std::vector<std::unique_ptr<Element>> newrow;
		for (auto& elem : row) {
			CompositeElement ce(*elem, **melem, std::minus<int>(), '-');
			newrow.push_back(std::unique_ptr<Element>(ce.clone()));
			melem++;
		}
		ssm.elements.push_back(std::move(newrow));
		mrow++;
	}
	
	return ssm;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator+(const SymbolicSquareMatrix& m) {
	
	if (n!=m.n) {
		throw std::domain_error("Matrix dimensions do not match");		
	}
	
	SymbolicSquareMatrix ssm;
	ssm.n = n;
	
	auto mrow = m.elements.begin();
	for (auto& row : elements) {
		auto melem = mrow->begin();
		std::vector<std::unique_ptr<Element>> newrow;
		for (auto& elem : row) {
			CompositeElement ce(*elem, **melem, std::plus<int>(), '+');
			newrow.push_back(std::unique_ptr<Element>(ce.clone()));
			melem++;
		}
		ssm.elements.push_back(std::move(newrow));
		mrow++;
	}
	
	return ssm;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator*(const SymbolicSquareMatrix& m) {
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	SymbolicSquareMatrix rightMat = m.transpose();
	std::vector<std::vector<std::unique_ptr<Element>> > newElements{};
	
	SymbolicSquareMatrix ssm;
	ssm.n = m.n;
	
	for (auto& lrow : elements) {
		std::vector<std::unique_ptr<Element>> nextRow{};
		
		for(auto& row: rightMat.elements) {
			
			nextRow.push_back( std::inner_product(row.begin(), row.end(), lrow.begin(),
				std::unique_ptr<Element>(new IntElement{0}),
				[] (const std::unique_ptr<Element>& p1, const std::unique_ptr<Element>& p2) {
					return std::unique_ptr<Element>(new CompositeElement(*p1,*p2, std::plus<int>(), '+'));},
				[] (const std::unique_ptr<Element>& m1, const std::unique_ptr<Element>& m2) {
					return std::unique_ptr<Element>(new CompositeElement(*m1,*m2, std::multiplies<int>(), '*'));
				}));
		}
		
		newElements.push_back(std::move(nextRow));		
	}
		
	ssm.elements = std::move(newElements);	
	
	return ssm;
}
		
bool SymbolicSquareMatrix::operator==(const SymbolicSquareMatrix& m) const {
	
	return toString() == m.toString();
}

void SymbolicSquareMatrix::print(std::ostream& os) const {
	
	os << toString();
}

std::string SymbolicSquareMatrix::toString() const {

	std::stringstream ss;
	
	ss << "[";
	
	for(auto& row:elements) {
		ss << "[";
		
		bool firstRound = true;
		for(auto& elem:row) {
			if (!firstRound) {
				ss << ",";
			}
			ss << *elem;
			firstRound = false;
		}
		ss << "]";
	}
	ss << "]";
	
	return ss.str();
}

ConcreteSquareMatrix SymbolicSquareMatrix::evaluate(const Valuation& val) const{
	
	ConcreteSquareMatrix csm;	
	csm.n = n;
	
	for (auto& row : elements) {
		std::vector<std::unique_ptr<IntElement>> newrow;
		for (auto& elem : row) {			
			newrow.push_back(std::unique_ptr<IntElement>(new IntElement(elem->evaluate(val))));
		}
		csm.elements.push_back(std::move(newrow));
	}
	 
	 return csm;	
}

void SymbolicSquareMatrix::isSquareMatrix(const std::string &str) {
	char c;	
	int number;
	int matrix_columns = 0;
	int matrix_rows = 0;
	int matrix_expected_size = 0;
	
	std::stringstream input(str);
	
	input >> c;
	
	// Examine string input to verify correct square-matrix format
	// Example: ->[<-[1,2,3][4,5,6]] can not be anything else than [	
	if (!input.good() || c != '[')
		throw std::invalid_argument("String is not a square matrix.");
	
	input >> c;
	
	// While char is not ] input-reference: [[1,2,3][4,5,6]->]<-	
	while (c != ']') {
		
		// Return false if char not [ input-reference: [->[<-1,2,3][4,5,6]]
		if (!input.good() || c != '[')
			throw std::invalid_argument("String is not a square matrix.");
		
		std::vector<std::unique_ptr<Element>> row;	
		
		matrix_columns = 0;
		
		do {
			
			char symbol = input.peek();
			
			// Check if symbol is alpha or number				
			if (isalpha(symbol)) {
				input >> c;
				row.push_back(std::unique_ptr<Element>(new VariableElement(c)));
			}
			else {
				
				input >> number;
				
				if (!input.good()) {
					throw std::invalid_argument("String is not a square matrix.");
				}
				
				row.push_back(std::unique_ptr<Element>(new IntElement(number)));
			}
			
			matrix_columns++;
			
			input >> c;
			
			// Return false if input not , OR ]
			if (!input.good() || (c != ',' && c != ']'))
				throw std::invalid_argument("String is not a square matrix.");
						
		} while (c != ']');
		
		elements.push_back(std::move(row));
		
		// Get square matrix dimensions
		if (matrix_rows == 0)
			matrix_expected_size = matrix_columns;
			
		matrix_rows++;
		
		// Check square-matrix colums & rows are equal
		if (matrix_expected_size != matrix_columns)
			throw std::invalid_argument("String is not a square matrix.");
				
		input >> c;
		
		// Return false if input not [ or ]
		if (!input.good() || (c != '[' && c != ']'))
			throw std::invalid_argument("String is not a square matrix.");
	}
	
	// Check square-matrix dimensions are correct
	if (matrix_expected_size != matrix_rows)
		throw std::invalid_argument("String is not a square matrix.");
	
	// Check input ends, getline to get whitespaces
	std::string tail;
	std::getline(input, tail);
	
	if (tail != "" || isspace(tail.front())) {
		throw std::invalid_argument("String is not a square matrix.");		
	}
	
	n = matrix_expected_size;
}

std::ostream& operator<<(std::ostream& os, const SymbolicSquareMatrix& m) {
	
	os << m.toString();

	return os;
}
