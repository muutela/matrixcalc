/**
    \file varelement.h
    \brief Header for VariableElement class
*/
#ifndef VARELEMENT_H_INCLUDED
#define VARELEMENT_H_INCLUDED

#include "element.h"

#include <string>

/**
    \class VariableElement
    \brief A class for VariableElement
*/
class VariableElement : public Element {
	
	private:
		char val;
		
	public:
		
		/**
        	\brief Default constructor
    	*/
		VariableElement():val{}{};
		
		/**
        	\brief Parametric constructor
        	\param v, char to initialize VariableElement val
    	*/
		VariableElement(char v):val{v}{};
		
		/**
        	\brief Default destructor
    	*/
		~VariableElement() = default;
		
		/**
        	\brief Getter of val
        	\return val value as char
    	*/
		char getVal() const;
		
		/**
        	\brief Setter of val
        	\param val value as char
    	*/
		void setVal(char v);
		
		/**
        	\brief Clone VariableElement, overrides Element.clone()
        	\return Pointer to new Element
    	*/
		Element* clone() const override;
		
		/**
        	\brief Creates a string presentation of VariableElement, overrides Element.toString()
        	\return string representation of IntElement
    	*/
		std::string toString() const override;
		
		/**
        	\brief Retrieves int value from a map<char,int> char symbol, overrides Element.evaluate()
        	\param Valuation map<char, int>
        	\throws std::out_of_range if val is not an element in the map
        	\return int value mapped to char
    	*/
		int evaluate(const Valuation&) const override;
		
};

#endif
