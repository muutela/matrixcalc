#ifndef ELEMENTARYMATRIX_H_INCLUDED
#define ELEMENTARYMATRIX_H_INCLUDED

#include "element.h"

#include <vector>
#include <memory>
#include <ostream>

template <typename T>
class ElementarySquareMatrix {
	
	private:		
		int n;
		std::vector<std::vector<std::unique_ptr<T>> > elements{};
		
		friend ElementarySquareMatrix<Element>;
		
	public:
		
		/**
			\brief Default constructor
		*/
		ElementarySquareMatrix():n{0}{};
	
		/**
			\brief Parametric constructor
			\param valid string for initializing ElementarySquareMatrix "[[1,2][3,4]]"
			\throws std::invalid_argument if string is invalid
		*/		
		ElementarySquareMatrix(const std::string& s);		
	
		/**
        	\brief Copy constructor
    	*/
		ElementarySquareMatrix(const ElementarySquareMatrix<T>&);
		
		/**
        	\brief Move constructor
    	*/
		ElementarySquareMatrix(ElementarySquareMatrix<T>&&);
		
		/**
        	\brief Destructor
    	*/
		virtual ~ElementarySquareMatrix() = default;
		
    	/**
        	\brief Transpose ElementarySquareMatrix
        	\return Transposed ElementarySquareMatrix
    	*/
    	ElementarySquareMatrix transpose() const;
    	
    	/**
        	\brief Assignment operator for ElementarySquareMatrix
        	\param Matrix m to be assigned to object value
        	\return ElementarySquareMatrix
    	*/
    	ElementarySquareMatrix<T>& operator=(const ElementarySquareMatrix<T>& m);
    	
    	/**
        	\brief Move assignment operator for ElementarySquareMatrix
        	\param Matrix m to be assigned to object value
        	\return ElementarySquareMatrix
    	*/
    	ElementarySquareMatrix<T>& operator=(ElementarySquareMatrix<T>&& m);
    	
		/**
        	\brief Operator for adding ElementarySquareMatrices
        	\param ElementarySquareMatrix m to be added
        	\throws std::domain_error if matrices sizes do not match
        	\return Addition result as ElementarySquareMatrix
    	*/
		ElementarySquareMatrix<T>& operator +=(const ElementarySquareMatrix<T>& m);
		
		/**
        	\brief Operator for subtracting SquareMatrices
        	\param ElementarySquareMatrix m to subtract
        	\throws std::domain_error if matrices sizes do not match
        	\return Subtraction result as ElementarySquareMatrix
    	*/
		ElementarySquareMatrix<T>& operator -=(const ElementarySquareMatrix<T>& m);
		
		/**
        	\brief Operator for multiplying SquareMatrices
        	\param ElementarySquareMatrix m to multiply
        	\throws std::domain_error if matrices sizes do not match
        	\return Multiplication result as ElementarySquareMatrix
    	*/
		ElementarySquareMatrix<T>& operator *=(const ElementarySquareMatrix<T>& m);    	
    	
    	/**
		    \brief Operator for ElementarySquareMatrix addition
		    \param first member of ElementarySquareMatrix
		    \param second member of ElementarySquareMatrix
		    \return The product of SquareMatrices
		*/
		ElementarySquareMatrix<T> operator+(const ElementarySquareMatrix<T>& m);
		
		/**
		    \brief Operator for ElementarySquareMatrix subtraction
		    \param first member of ElementarySquareMatrix
		    \param second member of ElementarySquareMatrix
		    \return The product of SquareMatrices
		*/
		ElementarySquareMatrix<T> operator-(const ElementarySquareMatrix<T>& m);
		
		/**
		    \brief Operator for ElementarySquareMatrix multiplication
		    \param first member of ElementarySquareMatrix
		    \param second member of ElementarySquareMatrix
		    \return The product of SquareMatrices
		*/
		ElementarySquareMatrix<T> operator*(const ElementarySquareMatrix<T>& m);
    	/**
        	\brief Equals operator for ElementarySquareMatrix
        	\param Matrix m to be compared to object value
        	\return boolean
    	*/
    	bool operator==(const ElementarySquareMatrix& m) const;
    	
    	/**
        	\brief Print ElementarySquareMatrix string representation to ostream
        	\param stream to print into
    	*/    	
		void print(std::ostream&) const;
		
		/**
        	\brief Creates a string presentation of the ElementarySquareMatrix 
        	\return string presentation of ElementarySquareMatrix: [[1,2][3,4]]
    	*/    	
		std::string toString() const;
		
		/**
        	\brief Creates ElementarySquareMatrix from a symbolic [[a,b][c,d]] square matrix
        	\param Valuation map<char, int>
        	\return ElementarySquareMatrix: [[1,2][3,4]]
    	*/
		ElementarySquareMatrix<IntElement> evaluate(const Valuation& val) const;
		
		/**
        	\brief Check if input string is valid SquareMatrix
        	\param String to be checked
    	*/
    	void isSquareMatrix(const std::string &str);
	
};

using SymbolicSquareMatrix = ElementarySquareMatrix<Element>;
using ConcreteSquareMatrix = ElementarySquareMatrix<IntElement>;

// Kopio-konstruktori
template<typename T>
ElementarySquareMatrix<T>::ElementarySquareMatrix(const ElementarySquareMatrix<T>& m):n(m.n) {
	
	for (auto& row : m.elements) {
		std::vector<std::unique_ptr<T>> newrow;
		for (auto& elem : row) {
			T* elemPtr = dynamic_cast<T*>(elem->clone());
			newrow.push_back(std::unique_ptr<T>(elemPtr));
		}
		elements.push_back(std::move(newrow));
	}
}

template<typename T>
ElementarySquareMatrix<T>::ElementarySquareMatrix(ElementarySquareMatrix<T>&& m){
	
	n = std::move(m.n); 
	elements = std::move(m.elements);
}

template<typename T>
void ElementarySquareMatrix<T>::print(std::ostream& os) const {
	os << toString();
}

template<typename T>
std::string ElementarySquareMatrix<T>::toString() const {
	
	std::stringstream ss;
	
	ss << "[";
	
	for(auto& row:elements) {
		ss << "[";
		
		bool firstRound = true;
		for(auto& elem:row) {
			if (!firstRound) {
				ss << ",";
			}
			ss << *elem;
			firstRound = false;
		}
		ss << "]";
	}
	ss << "]";
	
	return ss.str();
}

template <typename T>
ElementarySquareMatrix<T> ElementarySquareMatrix<T>::transpose() const {

	ElementarySquareMatrix esm;
	
	std::vector<std::vector<std::unique_ptr<T>> > new_elts(n);		
	
	for (auto& row : elements) {
		int i = 0;
		for (auto& elem : row) {
			T* elemPtr = dynamic_cast<T*>(elem->clone());
			new_elts[i].push_back(std::unique_ptr<T>(elemPtr));
			i++;
		}		
	}
	
	esm.elements = std::move(new_elts);
	esm.n = n;
	
	return esm;	
}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator=(const ElementarySquareMatrix<T>& m) {
	
	n = m.n;
	ElementarySquareMatrix cpy(m);
	std::swap(elements, cpy.elements);
	
	return *this;
}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator=(ElementarySquareMatrix<T>&& m){
	
	if (this == &m) return *this;
	
	n = std::move(m.n);	
	elements = std::move(m.elements);
	
	return *this;
}

template <typename T>
bool ElementarySquareMatrix<T>::operator==(const ElementarySquareMatrix<T>& m) const {
	
	return toString() == m.toString();
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const ElementarySquareMatrix<T>& m) {
	
	os << m.toString();

	return os;
}

#endif
