/**
    \file tests.cpp
    \brief Tests for all classes
*/
#include "catch.hpp"
#include "valuation.h"
#include "element.h"
#include "elementarymatrix.h"
#include "compositeelement.h"

#include <sstream>
#include <ostream>
#include <string>

//  ################## VALUATION TESTS #########################
TEST_CASE("Valuation tests", "[true]") {
	
	Valuation valu;
	valu['a'] = 99;
	valu['b'] = 55;
	
	CHECK(valu.at('a')== 99);
	CHECK_THROWS(valu.at('x') == 55);
}

//  ################## ELEMENT TESTS #########################
TEST_CASE("Element basic tests", "[true]") {
	
	std::stringstream ss;
	VariableElement vare('k');
	IntElement ient(3);
	ss << vare << ient;
	CHECK(ss.str() == "k3");
	CHECK(vare.toString() == "k");
}

//  ################## INTELEMENT TESTS #########################
TEST_CASE("IntElement basic tests", "[true]"){
	
	IntElement zero;	
	CHECK(zero.getVal()==0);
	
	zero.setVal(-5);	
	CHECK(zero.getVal()==-5);
	
	IntElement *pElem = dynamic_cast<IntElement*>(zero.clone());
	CHECK(pElem->getVal() == -5);
	CHECK(pElem->toString() == "-5");
	delete pElem;
	
	//Valuation valu;
	//valu['a'] = 99;
	//CHECK(zero.evaluate(valu) == 99); hämmentävä funktio	
}

TEST_CASE("IntElement operators test", "[true & false]"){
	
	IntElement e10{10};
	IntElement e25{25};
	IntElement res{0};
	res = e10 + e25;
	CHECK(res.getVal()==35);
	
	res.setVal(0);	
	res = e10 - e25;
	CHECK(res.getVal()==-15);
	
	IntElement e5{5};
	IntElement e6{6};
	e5 *= e6;
	CHECK(e5.getVal() == 30);
	
	IntElement eq1{6};
	IntElement eq2{6};
	CHECK(eq1 == eq2);
	CHECK_FALSE(eq1 == e5);
}

//  ################## VARIABLEELEMENT TESTS #########################
TEST_CASE("VariableElement basic & constructor tests", "[true & false]"){
	
	VariableElement vare('k');
	CHECK(vare.getVal() == 'k');
	VariableElement vare2;
	vare2.setVal('s');
	CHECK(vare2.getVal() == 's');
	CHECK_FALSE(vare2.getVal() == 'd');
	
	Valuation valu;
	valu['a'] = 99;
	valu['b'] = 55;
	valu['k'] = 5;
	CHECK(vare.evaluate(valu) == 5);
	CHECK_NOTHROW(vare.evaluate(valu) == 5);
	CHECK_THROWS(vare2.evaluate(valu) == 5);
	
	VariableElement vare3('k');
	CHECK(vare == vare3);
	
	std::string str = vare3.toString();
	CHECK(str == vare3.toString());
	CHECK_FALSE(str == vare2.toString());	
}

//  ################## COMPOSITEELEMENT TESTS #########################
TEST_CASE("CompositeElement basic & constructor tests", "[true & false]"){
	
	auto plusFun = std::plus<int>();
	auto minusFun = std::minus<int>();
	auto multFun = std::multiplies<int>();
	
	Valuation valu;
	valu['x'] = 10;
	valu['a'] = 10;
	valu['b'] = 20;
	valu['c'] = 30;
	
	VariableElement v1('b');
	VariableElement v2('x');
	
	IntElement i1(5), i2(3);
	
	CompositeElement ce1(v1,v2, plusFun, '+');
	CompositeElement ce2(v1,i2, minusFun, '-');
	CompositeElement ce3(i1,i2, multFun, '*');
	
	CHECK(ce1.evaluate(valu) == 30);
	CHECK(ce2.evaluate(valu) == 17);
	CHECK(ce3.evaluate(valu) == 15);
	
	CompositeElement ce4(ce1, ce2, multFun, '*');
	CHECK(ce4.evaluate(valu) == 510);
	CHECK(ce4.toString() == "((b+x)*(b-3))");
	
	CompositeElement ce5(ce1, ce2, plusFun, '+');
	CHECK(ce5.evaluate(valu) == 47);
	CHECK(ce5.toString() == "((b+x)+(b-3))");
	
	CompositeElement ce6(ce3, ce2, minusFun, '-');
	CHECK(ce6.evaluate(valu) == -2);
	CHECK(ce6.toString() == "((5*3)-(b-3))");
	
	CompositeElement ce7(ce6);
	CHECK(ce7.toString() == ce6.toString());
	ce7 = ce5;
	CHECK(ce7.toString() == ce5.toString());
}

//  ############### CONCRETESQUAREMATRIX TESTS ###################
TEST_CASE("ConcreteMatrix constructors","[correct format]"){
	
	CHECK_NOTHROW(ConcreteSquareMatrix("[[1]]"));
	CHECK_NOTHROW(ConcreteSquareMatrix("[[1,-2][1,4]]"));
	CHECK_NOTHROW(ConcreteSquareMatrix("[[1,2,3][1,4,-5][2,7,8]]"));	
    CHECK_NOTHROW(ConcreteSquareMatrix("[[1,-2,3,4][1,4,5,6][2,7,8,9][7,3,-5,8]]"));
	
	ConcreteSquareMatrix m{"[[5,6][7,9]]"};
	CHECK(m == ConcreteSquareMatrix("[[5,6][7,9]]"));
	
	ConcreteSquareMatrix m1(m);
	CHECK(m == m1);
	
	std::string str = m.toString();
	ConcreteSquareMatrix mv(std::move(m));
	CHECK(str == mv.toString());
}

TEST_CASE("ConcreteSquareMatrix bad","[ill-formed]"){
	
	CHECK_THROWS(ConcreteSquareMatrix("[[1][2,3]]"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,2][3]]"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,2,4][3,3,5]]"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,2][4,5]"));
	CHECK_THROWS(ConcreteSquareMatrix("[1,3][2,3]]"));
	CHECK_THROWS(ConcreteSquareMatrix("1,3][2,3]]"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,3][2,3"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1 3][2,3]]"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,-33][2,3,5]]"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,-33][0,3]]-"));
	CHECK_THROWS(ConcreteSquareMatrix("[[1,-33][0,3]] "));
		
	ConcreteSquareMatrix sm1("[[1,2][2,3]]");
	ConcreteSquareMatrix sm2("[[1,2,3][3,4,6][3,4,6]]");
	CHECK_THROWS(sm1 += sm2);
	CHECK_THROWS(sm1 -= sm2);
	CHECK_THROWS(sm1 *= sm2);
}

TEST_CASE("ConcreteSquareMatrix operators I", "[true]") {
	
	ConcreteSquareMatrix sm0("[[0,0][0,0]]");
	ConcreteSquareMatrix sm1("[[1,-2][1,4]]");
	ConcreteSquareMatrix sm2("[[1,-2][1,4]]");
	
	sm0 -= sm1;
	CHECK(sm1.toString() == "[[1,-2][1,4]]");
	
	sm1 += sm2;	
	CHECK(sm1.toString() == "[[2,-4][2,8]]");
	
	sm0 = sm2;
	CHECK(sm0.toString() == sm2.toString());
	CHECK(sm0 == sm2);
	
	sm0 = sm1 + sm2;
	CHECK(sm0.toString() == "[[3,-6][3,12]]");
	
	std::stringstream test;	
	test << sm0;	
	CHECK(sm0.toString() == test.str());
	
	ConcreteSquareMatrix sm3("[[1,2,3][1,4,5][2,7,8]]");
	ConcreteSquareMatrix sm4("[[1,1,2][2,4,7][3,5,8]]");
	ConcreteSquareMatrix trn = sm3.transpose();
	CHECK(trn.toString() == sm4.toString());
	
	ConcreteSquareMatrix sm5("[[2,2][3,3]]");
	ConcreteSquareMatrix sm6("[[1,2][3,4]]");
	sm5 *= sm6;
	CHECK(sm5.toString() == "[[8,12][12,18]]");
	
	ConcreteSquareMatrix sm7("[[1,2][3,4]]");
	ConcreteSquareMatrix sm8("[[5,6][7,8]]");
	sm7 *= sm8;	
	CHECK(sm7.toString() == "[[19,22][43,50]]");
	
	ConcreteSquareMatrix sm9("[[1,2,3][5,3,4][12,3,-5]]");
	ConcreteSquareMatrix smx("[[0,2,-3][5,33,4][14,3,-5]]");
	ConcreteSquareMatrix res = sm9 * smx;
	CHECK(res.toString() == "[[52,77,-10][71,121,-23][-55,108,1]]");
	
	ConcreteSquareMatrix smxi(res);
	ConcreteSquareMatrix smxii(res);
	res = smxi - smxii;
	CHECK(res.toString() == "[[0,0,0][0,0,0][0,0,0]]");
	
	ConcreteSquareMatrix sm_eq(sm9);
	CHECK(sm_eq == sm9);
}

TEST_CASE("ConcreteSquareMatrix operators II", "[false]") {
	
	ConcreteSquareMatrix sm0("[[0,0][0,0]]");
	ConcreteSquareMatrix sm1("[[1,-2][1,4]]");
	ConcreteSquareMatrix sm2("[[1,-2][1,4]]");
	
	sm0 -= sm1;
	CHECK_FALSE(sm1.toString() == "[[1,-2][-1,-4]]");
	
	sm1 += sm2;	
	CHECK_FALSE(sm1.toString() == "[[0,-4][2,8]]");	
	
	sm0 = sm1;
	CHECK_FALSE(sm0.toString() == sm2.toString());
	
	sm1 *= sm0;
	CHECK_FALSE(sm1.toString() == "[[0,-4][2,8]]");
	
	ConcreteSquareMatrix sm9("[[1,2,3][5,3,4][12,3,-5]]");
	ConcreteSquareMatrix smx("[[0,2,-3][5,33,4][14,3,-5]]");
	ConcreteSquareMatrix res = sm9 + smx;
	CHECK_FALSE(res.toString() == "[[1,4,1][10,36,8][26,6,-10]]");
}

TEST_CASE("ConcreteSquareMatrix toString, print test I", "[true]") {
	
	ConcreteSquareMatrix sm1("[[1,-2][1,4]]");
	CHECK(sm1.toString() == "[[1,-2][1,4]]");
	
	std::stringstream ss;
	sm1.print(ss);
	CHECK(ss.str() == sm1.toString());
}

TEST_CASE("ConcreteSquareMatrix toString test II", "[false]") {
	
	ConcreteSquareMatrix sm1("[[1,-2][1,4]]");
	CHECK_FALSE(sm1.toString() == "[[1,-2][1,4]] ");
}

//  ############### SYMBOLICSQUAREMATRIX TESTS ###################
TEST_CASE("SymbolicMatrix constructors good","[correct format]"){	
		
	CHECK_NOTHROW(SymbolicSquareMatrix("[[a]]"));
	CHECK_NOTHROW(SymbolicSquareMatrix("[[1,c][1,x]]"));
	CHECK_NOTHROW(SymbolicSquareMatrix("[[1,e,3][1,4,k][2,v,i]]"));	
	
	SymbolicSquareMatrix m{"[[5,x][7,b]]"};
	CHECK(m == SymbolicSquareMatrix("[[5,x][7,b]]"));
	
	SymbolicSquareMatrix m1(m);
	CHECK(m == m1);
	
	std::string str = m.toString();
	SymbolicSquareMatrix mv(std::move(m));
	CHECK(str == mv.toString());	
}

TEST_CASE("SymbolicSquareMatrix constructors bad","[ill-formed]"){
	
	CHECK_THROWS(SymbolicSquareMatrix("[[d,d][-e,a]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[d][e,a]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[d,l][m]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[1,d,4][3,w,5]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[1,h][4,q]"));
	CHECK_THROWS(SymbolicSquareMatrix("[1,3][a,o]]"));
	CHECK_THROWS(SymbolicSquareMatrix("1,3][2,3]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[1,l][2,s"));
	CHECK_THROWS(SymbolicSquareMatrix("[[1 r][2,c]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[1,-T][2,H,5]]"));
	CHECK_THROWS(SymbolicSquareMatrix("[[Q,X][a,7]]-"));
	CHECK_THROWS(SymbolicSquareMatrix("[[1,m][0,3]] "));
}

TEST_CASE("SymbolicMatrix evaluate tests","[correct format]"){
	
	Valuation val;
	val['a']= 1;
	val['b']= 2;
	val['c']= 3;
	val['d']= 4;
	
	SymbolicSquareMatrix ssm("[[a,b][c,d]]");
	ConcreteSquareMatrix csm = ssm.evaluate(val);
	CHECK(csm.toString() == "[[1,2][3,4]]");
	
	SymbolicSquareMatrix ssm1("[[a,b,0][0,-55,c][d,-2,b]]");
	csm = ssm1.evaluate(val);
	CHECK(csm.toString() == "[[1,2,0][0,-55,3][4,-2,2]]");
}

TEST_CASE("SymbolicSquareMatrix operators","[true]"){	
	
	SymbolicSquareMatrix sm1("[[a,2][2,3]]");
	SymbolicSquareMatrix sm2("[[1,2,l][3,e,6][z,4,6]]");
	sm1 = sm2;
	CHECK(sm1 == sm2);
	
	std::stringstream ss;
	sm1.print(ss);
	CHECK(ss.str() == sm1.toString());
	
	SymbolicSquareMatrix sm3("[[1,b,c][1,4,5][b,7,x]]");
	SymbolicSquareMatrix sm4("[[1,1,b][b,4,7][c,5,x]]");
	SymbolicSquareMatrix trn = sm3.transpose();
	CHECK(trn.toString() == sm4.toString());
	
	SymbolicSquareMatrix sm5("[[Q,b,X][1,s,5][b,7,x]]");
	sm4 = std::move(sm5);
	CHECK(sm4.toString() == "[[Q,b,X][1,s,5][b,7,x]]");
	
	ss.str(std::string());
	SymbolicSquareMatrix ssm0("[[1,2][2,x]]");
	SymbolicSquareMatrix ssm1("[[x,y][z,-100]]");
	ss << ssm0 << ssm1;
	CHECK(ss.str() == "[[1,2][2,x]][[x,y][z,-100]]");
	
	// CALCULATION TESTS
	Valuation val;
	val['a']= 0;
	val['b']= 1;
	val['c']= 2;
	val['d']= 3;
	val['e']= 4;
	val['f']= 5;
	
	SymbolicSquareMatrix sm7("[[f,b,c][1,-4,5][b,7,e]]");
	SymbolicSquareMatrix sm8("[[1,5,b][b,a,7][c,5,d]]");
	SymbolicSquareMatrix res;	
	
	res = sm7 + sm8;
	CHECK(res.evaluate(val) == ConcreteSquareMatrix("[[6,6,3][2,-4,12][3,12,7]]"));
	CHECK(res.toString() == "[[(f+1),(b+5),(c+b)][(1+b),(-4+a),(5+7)][(b+c),(7+5),(e+d)]]");
	
	res = sm7 - sm8;
	CHECK(res.evaluate(val) == ConcreteSquareMatrix("[[4,-4,1][0,-4,-2][-1,2,1]]"));
	ConcreteSquareMatrix csm("[[4,-4,1][0,-4,-2][-1,2,1]]");
	CHECK(res.toString() == "[[(f-1),(b-5),(c-b)][(1-b),(-4-a),(5-7)][(b-c),(7-5),(e-d)]]");
	CHECK(res.evaluate(val).toString() == csm.toString());
	
	res = sm7 * sm8;
	CHECK(res.evaluate(val) == ConcreteSquareMatrix("[[10,35,18][7,30,-12][16,25,62]]"));
	
	// INVALID DIMENSIONS
	SymbolicSquareMatrix inv("[[1,5][b,7]]");
	CHECK_THROWS(res = sm7 + inv);
	CHECK_THROWS(res = sm7 - inv);
	CHECK_THROWS(res = sm7 * inv);	
}

TEST_CASE("SymbolicSquareMatrix toString, print test I", "[true]") {
	
	SymbolicSquareMatrix sm1("[[a,-2][d,4]]");
	CHECK(sm1.toString() == "[[a,-2][d,4]]");
	
	std::stringstream ss;
	sm1.print(ss);
	CHECK(ss.str() == sm1.toString());
}

TEST_CASE("SymbolicSquareMatrix toString test II", "[false]") {
	
	SymbolicSquareMatrix sm1("[[1,-2][1,d]]");
	CHECK_FALSE(sm1.toString() == "[[1,-2][1,4]]");
}

