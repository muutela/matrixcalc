/**
    \file element.cpp
    \brief Element class code
*/
#include "element.h"

#include <ostream>

template <>
TElement<int>::TElement() {
	val = 0;
}

template <>
TElement<char>::TElement() {
	val = ' ';
}

std::ostream& operator<<(std::ostream& os, const Element& e){
	
	os << e.toString();
	
	return os;
}

bool operator==(const Element& e1, const Element& e2) {
	
	return e1.toString() == e2.toString();
}

template <>
TElement<int>& TElement<int>::operator+=(const TElement<int>& i) {
	val += i.val;
	return *this;
}

template <>
TElement<int>& TElement<int>::operator-=(const TElement<int>& i) {
	val -= i.val;
	return *this;
}

template <>
TElement<int>& TElement<int>::operator*=(const TElement<int>& i) {
	val *= i.val;
	return *this;	
}

IntElement operator+(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum += j;
	return sum;
}

IntElement operator-(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum -= j;
	return sum;
}

IntElement operator*(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum *= j;
	return sum;
}

template <>
int TElement<int>::evaluate(const Valuation& v) const {
	
	return val;
}

template <>
int TElement<char>::evaluate(const Valuation& v) const {

	return v.at(val);
}
