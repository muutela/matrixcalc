
#include "element.h"
#include "elementarymatrix.h"
#include "compositeelement.h"

#include <ostream>
#include <numeric>
#include <algorithm>

template <>
void ElementarySquareMatrix<Element>::isSquareMatrix(const std::string &str) {

	char c;	
	int number;
	int matrix_columns = 0;
	int matrix_rows = 0;
	int matrix_expected_size = 0;
	
	std::stringstream input(str);
	
	input >> c;
	
	// Examine string input to verify correct square-matrix format
	// Example: ->[<-[1,2,3][4,5,6]] can not be anything else than [	
	if (!input.good() || c != '[')
		throw std::invalid_argument("String is not a square matrix.");
	
	input >> c;
	
	// While char is not ] input-reference: [[1,2,3][4,5,6]->]<-	
	while (c != ']') {
		
		// Return false if char not [ input-reference: [->[<-1,2,3][4,5,6]]
		if (!input.good() || c != '[')
			throw std::invalid_argument("String is not a square matrix.");
		
		std::vector<std::unique_ptr<Element>> row;	
		
		matrix_columns = 0;
		
		do {
			
			char symbol = input.peek();
			
			// Check if symbol is alpha or number				
			if (isalpha(symbol)) {
				input >> c;
				row.push_back(std::unique_ptr<Element>(new VariableElement(c)));
			}
			else {
				
				input >> number;
				
				if (!input.good()) {
					throw std::invalid_argument("String is not a square matrix.");
				}
				
				row.push_back(std::unique_ptr<Element>(new IntElement(number)));
			}
			
			matrix_columns++;
			
			input >> c;
			
			// Return false if input not , OR ]
			if (!input.good() || (c != ',' && c != ']'))
				throw std::invalid_argument("String is not a square matrix.");
						
		} while (c != ']');
		
		elements.push_back(std::move(row));
		
		// Get square matrix dimensions
		if (matrix_rows == 0)
			matrix_expected_size = matrix_columns;
			
		matrix_rows++;
		
		// Check square-matrix colums & rows are equal
		if (matrix_expected_size != matrix_columns)
			throw std::invalid_argument("String is not a square matrix.");
				
		input >> c;
		
		// Return false if input not [ or ]
		if (!input.good() || (c != '[' && c != ']'))
			throw std::invalid_argument("String is not a square matrix.");
	}
	
	// Check square-matrix dimensions are correct
	if (matrix_expected_size != matrix_rows)
		throw std::invalid_argument("String is not a square matrix.");
	
	// Check input ends, getline to get whitespaces
	std::string tail;
	std::getline(input, tail);
	
	if (tail != "" || isspace(tail.front())) {
		throw std::invalid_argument("String is not a square matrix.");		
	}
	
	n = matrix_expected_size;
}

template <>
void ElementarySquareMatrix<IntElement>::isSquareMatrix(const std::string &str) {
	
	char c;	
	int number;
	int matrix_columns = 0;
	int matrix_rows = 0;
	int matrix_expected_size = 0;
	
	std::stringstream input(str);
	
	input >> c;
	
	// Examine string input to verify correct square-matrix format
	// Example: ->[<-[1,2,3][4,5,6]] can not be anything else than [	
	if (!input.good() || c != '[')
		throw std::invalid_argument("String is not a square matrix.");
	
	input >> c;
	
	// While char is not ] input-reference: [[1,2,3][4,5,6]->]<-	
	while (c != ']') {
		
		// Return false if char not [ input-reference: [->[<-1,2,3][4,5,6]]
		if (!input.good() || c != '[')
			throw std::invalid_argument("String is not a square matrix.");
		
		std::vector<std::unique_ptr<IntElement>> row;	
		
		matrix_columns = 0;
		
		do {
			
			input >> number;
			
			// char symbol = stream.peek();
			
			// Return false if input not integer, [[->1<-, 2,3]
			if (!input.good())
				throw std::invalid_argument("String is not a square matrix.");
			
			//row.push_back(IntElement{number});
			row.push_back(std::unique_ptr<IntElement>(new IntElement(number)));
			matrix_columns++;
			
			input >> c;
			
			// Return false if input not , OR ]
			if (!input.good() || (c != ',' && c != ']'))
				throw std::invalid_argument("String is not a square matrix.");
						
		} while (c != ']');
		
		elements.push_back(std::move(row));
		
		// Get square matrix dimensions
		if (matrix_rows == 0)
			matrix_expected_size = matrix_columns;
			
		matrix_rows++;
		
		// Check square-matrix colums & rows are equal
		if (matrix_expected_size != matrix_columns)
			throw std::invalid_argument("String is not a square matrix.");
				
		input >> c;
		
		// Return false if input not [ or ]
		if (!input.good() || (c != '[' && c != ']'))
			throw std::invalid_argument("String is not a square matrix.");	
	}
	
	// Check square-matrix dimensions are correct
	if (matrix_expected_size != matrix_rows)
		throw std::invalid_argument("String is not a square matrix.");
	
	// Check input ends, getline to get whitespaces
	std::string tail;
	std::getline(input, tail);
	
	if (tail != "" || isspace(tail.front())) {
		throw std::invalid_argument("String is not a square matrix.");		
	}
	
	n = matrix_expected_size;
}

template <>
ElementarySquareMatrix<Element>::ElementarySquareMatrix(const std::string& s) {
	
	isSquareMatrix(s);
}

template <>
ElementarySquareMatrix<IntElement>::ElementarySquareMatrix(const std::string& s) {

	isSquareMatrix(s);
}

// Concrete
template <>
ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator +=(const ElementarySquareMatrix<IntElement>& m){
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	auto m_row = m.elements.begin();
	
	for (auto& row : elements) {
		std::transform(row.begin(), row.end(),
			m_row->begin(), row.begin(),
			[] (
			const std::unique_ptr<IntElement> &p1, const std::unique_ptr<IntElement> &p2) {
			IntElement sum = *p1+*p2; return std::unique_ptr<IntElement>(new IntElement(sum));
			});
		m_row++;
	}
	
	return *this;
}

template <>
ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator -=(const ElementarySquareMatrix<IntElement>& m){
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	auto m_row = m.elements.begin();
	
	for (auto& row : elements) {
		std::transform(row.begin(), row.end(),
			m_row->begin(), row.begin(),
			[] (
			const std::unique_ptr<IntElement> &p1, const std::unique_ptr<IntElement> &p2) {
			IntElement sum = *p1-*p2; return std::unique_ptr<IntElement>(new IntElement(sum));
			});
		m_row++;
	}
	
	return *this;
}

template <>
ElementarySquareMatrix<IntElement>& ElementarySquareMatrix<IntElement>::operator *=(const ElementarySquareMatrix<IntElement>& m){
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	ConcreteSquareMatrix rightMat = m.transpose();
	std::vector<std::vector<std::unique_ptr<IntElement>> > newElements{};
	
	for (auto& lrow : elements) {
		std::vector<std::unique_ptr<IntElement>> nextRow{};
		
		for(auto& row: rightMat.elements) {
			std::vector<IntElement> prodRow(n);
			std::transform( row.begin(), row.end(), lrow.begin(), prodRow.begin(),
				[] (const std::unique_ptr<IntElement>& p1, const std::unique_ptr<IntElement>& p2) {	
				return (*p1)*(*p2); });
			IntElement sum = std::accumulate(prodRow.begin(), prodRow.end(),
				IntElement{0}, std::plus<IntElement>());
			nextRow.push_back(std::unique_ptr<IntElement>(new IntElement(sum))); // kloonaa summa, unique_ptr
		}
		
		newElements.push_back(std::move(nextRow));
		
	}
	
	elements = std::move(newElements);
	
	return *this;	
}   	

// Concrete
template <>
ElementarySquareMatrix<IntElement> ElementarySquareMatrix<IntElement>::operator+(const ElementarySquareMatrix<IntElement>& m){
		
	ElementarySquareMatrix esm(m);
	return *this += m;
}

template <>
ElementarySquareMatrix<IntElement> ElementarySquareMatrix<IntElement>::operator-(const ElementarySquareMatrix<IntElement>& m){
	
	ElementarySquareMatrix esm(m);
	return *this -= m;
}

template <>
ElementarySquareMatrix<IntElement> ElementarySquareMatrix<IntElement>::operator*(const ElementarySquareMatrix<IntElement>& m){
	ElementarySquareMatrix esm(m);
	return *this *= m;
}

// Symbolic
template <>
ElementarySquareMatrix<Element> ElementarySquareMatrix<Element>::operator+(const ElementarySquareMatrix<Element>& m){
	
	if (n!=m.n) {
		throw std::domain_error("Matrix dimensions do not match");		
	}
	
	SymbolicSquareMatrix ssm;
	ssm.n = n;
	
	auto mrow = m.elements.begin();
	for (auto& row : elements) {
		auto melem = mrow->begin();
		std::vector<std::unique_ptr<Element>> newrow;
		for (auto& elem : row) {
			CompositeElement ce(*elem, **melem, std::plus<int>(), '+');
			newrow.push_back(std::unique_ptr<Element>(ce.clone()));
			melem++;
		}
		ssm.elements.push_back(std::move(newrow));
		mrow++;
	}
	
	return ssm;
}

template <>
ElementarySquareMatrix<Element> ElementarySquareMatrix<Element>::operator-(const ElementarySquareMatrix<Element>& m){
	
	if (n!=m.n) {
		throw std::domain_error("Matrix dimensions do not match");		
	}
	
	SymbolicSquareMatrix ssm;
	ssm.n = n;
	
	auto mrow = m.elements.begin();
	for (auto& row : elements) {
		auto melem = mrow->begin();
		std::vector<std::unique_ptr<Element>> newrow;
		for (auto& elem : row) {
			CompositeElement ce(*elem, **melem, std::minus<int>(), '-');
			newrow.push_back(std::unique_ptr<Element>(ce.clone()));
			melem++;
		}
		ssm.elements.push_back(std::move(newrow));
		mrow++;
	}
	
	return ssm;
}

template <>
ElementarySquareMatrix<Element> ElementarySquareMatrix<Element>::operator*(const ElementarySquareMatrix<Element>& m){
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	ElementarySquareMatrix<Element> rightMat = m.transpose();
	std::vector<std::vector<std::unique_ptr<Element>> > newElements{};
	
	ElementarySquareMatrix<Element> esm;
	esm.n = m.n;
	
	for (auto& lrow : elements) {
		std::vector<std::unique_ptr<Element>> nextRow{};
		
		for(auto& row: rightMat.elements) {
			
			nextRow.push_back( std::inner_product(row.begin(), row.end(), lrow.begin(),
				std::unique_ptr<Element>(new IntElement{0}),
				[] (const std::unique_ptr<Element>& p1, const std::unique_ptr<Element>& p2) {
					return std::unique_ptr<Element>(new CompositeElement(*p1,*p2, std::plus<int>(), '+'));},
				[] (const std::unique_ptr<Element>& m1, const std::unique_ptr<Element>& m2) {
					return std::unique_ptr<Element>(new CompositeElement(*m1,*m2, std::multiplies<int>(), '*'));
				}));
		}
		
		newElements.push_back(std::move(nextRow));		
	}
		
	esm.elements = std::move(newElements);	
	
	return esm;
}

template <>
ElementarySquareMatrix<IntElement> ElementarySquareMatrix<Element>::evaluate(const Valuation& v) const {
	
	ElementarySquareMatrix<IntElement> esm;	
	esm.n = n;
	
	for (auto& row : elements) {
		std::vector<std::unique_ptr<IntElement>> newrow;
		for (auto& elem : row) {			
			newrow.push_back(std::unique_ptr<IntElement>(new IntElement(elem->evaluate(v))));
		}
		esm.elements.push_back(std::move(newrow));
	}
	 
	 return esm;
}

template <>
ElementarySquareMatrix<IntElement> ElementarySquareMatrix<IntElement>::evaluate(const Valuation& v) const {
	
	return *this;
}

