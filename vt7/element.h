/**
    \file element.h
    \brief Header for Element class
*/
#ifndef ELEMENT_H_INCLUDED
#define ELEMENT_H_INCLUDED

#include "valuation.h"

#include <ostream>
#include <string>
#include <sstream>

/**
    \class Element
    \brief A class for Element
*/
class Element {
	
	public:
		/**
        	\brief Destructor
    	*/
		virtual ~Element() = default;
		
		/**
        	\brief Clone Element, overridden by TElement
        	\return Pointer to new Element
    	*/
		virtual Element* clone() const = 0;
		
		/**
        	\brief Creates a string presentation of Element, overridden by TElement
        	\return string representation of Element
    	*/
		virtual std::string toString() const = 0;
		
		/**
        	\brief Retrieves int value from a map<char,int> char symbol, overridden by TElement
        	\param Valuation map<char, int>
        	\return int value mapped to char
    	*/
		virtual int evaluate(const Valuation& v) const = 0;
};

/**
    \brief Output operator
    \param osstream to print in
    \param Element to print from
    \return ostream
*/
std::ostream& operator<<(std::ostream& os, const Element& e);

/**
    \brief Equals operator
    \param Element 1 to compare
    \param Element 2 to compare
    \return ostream
*/
bool operator==(const Element&, const Element&);

/**
    \class TElement
    \brief A class for TElement, extends Element
*/
template <typename Type>
class TElement : public Element {
	
	private:
		Type val;
		
	public:
		
		/**
        	\brief Default constructor
    	*/
		TElement();
		
		/**
        	\brief Parametric constructor
        	\param Type t, initializer of val
    	*/
		TElement(Type t):val{t}{}
		
		/**
        	\brief Destructor
    	*/
		virtual ~TElement() = default;
		
		/**
        	\brief Getter of val
        	\return Type val
    	*/
		Type getVal() const {
			return val;
		}
		
		/**
        	\brief Setter of val
        	\param Type t val
    	*/
		void  setVal(Type t) {
			val = t;
		}
		
		virtual Element* clone() const override {			
			return new TElement<Type>(*this);
		}
		
		virtual std::string toString() const override {
			
			std::stringstream ss;
			ss << getVal();
			
			return ss.str();
		}    	
		
		/**
        	\brief Retrieves int value from a map<char,int> char symbol
        	\param Valuation map<char, int>
        	\return int value mapped to char
    	*/
		virtual int evaluate(const Valuation& v) const override;
		
		/**
        	\brief Operator for adding TElements
        	\param const TElement to add
        	\return Addition result as TElement
    	*/
		TElement<Type>& operator+=(const TElement<Type>& i);
		
		/**
        	\brief Operator for subtracting TElements
        	\param const TElement to subtract
        	\return Subtraction result as TElement
    	*/		
		TElement<Type>& operator-=(const TElement<Type>& i);
		
		/**
        	\brief Operator for multiplying TElements
        	\param const TElement to add
        	\return Multiplication result as TElement
    	*/		
		TElement<Type>& operator*=(const TElement<Type>& i);
};

using IntElement = TElement<int>;
using VariableElement = TElement<char>;

/**
    \brief Operator for IntElement addition
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator+(const IntElement& i, const IntElement& j);

/**
    \brief Operator for IntElement subtraction
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator-(const IntElement& i, const IntElement& j);

/**
    \brief Operator for IntElement multiplication
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator*(const IntElement& i, const IntElement& j);

#endif
