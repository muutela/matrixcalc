#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "elementarymatrix.h"
#include "element.h"
#include "valuation.h"
#include "compositeelement.h"

#include <sstream>
#include <ostream>
#include <string>
#include <iostream>
#include <stack>
#include <exception>

void printUserInterface();
std::string getUserInput();
int processInputType(std::string);
void calculate(int);

enum inputType {
	EMPTY = 0,
	MATRIX = 1,
	PLUS = 2, 
	MINUS = 3,
	MULTIPLY = 4, 
	EVALUATE = 5,
	NEWEVAL = 6,
	INVALID = 7,
	UNDEFINED = 8,
	QUIT = 9
};
int type = EMPTY;

Valuation valu;
std::stack<SymbolicSquareMatrix> stck;

int main() {
	
	int result = Catch::Session().run();
	
	if (result != 0) {
		std::cout << "Catch session fail" << std::endl;
	}		
	
	std::stringstream ss;
	std::string input = "";	
	
	while (input != "quit") {
		
		printUserInterface();
		input = getUserInput();
		
		try {
			type = processInputType(input);
		}
		catch (std::exception& e){
			type = UNDEFINED;
		}		
		
		switch (type) {
			case EMPTY:
				std::cout << std::endl;
				std::cout << "Empty input string, use valid input format.";
				std::cout << std::endl;
				break;
			case MATRIX:
				std::cout << std::endl;			
				std::cout << " PINOON:> " << stck.top().toString();
				std::cout << std::endl;
				break;
			case PLUS:
				std::cout << std::endl;				
				calculate(type);
				std::cout << " PLUS >> TULOS PINOON:> " << stck.top().toString();
				std::cout << std::endl;			
				break;
			case MINUS:
				std::cout << std::endl;
				calculate(type);
				std::cout << " MINUS >> TULOS PINOON:> " << stck.top().toString();
				std::cout << std::endl;			
				break;
			case MULTIPLY:
				std::cout << std::endl;;
				calculate(type);
				std::cout << "MULTIPLY >> TULOS PINOON:> " << stck.top().toString();
				std::cout << std::endl;				
				break;
			case EVALUATE:
				std::cout << std::endl;				
				try {
					std::cout << " MATRIX: " << stck.top().toString() << std::endl;
					std::cout << " >> EVALUATE: " << stck.top().evaluate(valu).toString();
				}
				catch (std::out_of_range& e) {
					std::cout << "Error: " << e.what() << ", no values mapped <char, int>" << std::endl;
				}				
				std::cout << std::endl;
				break;
			case NEWEVAL:
				std::cout << std::endl;	
				std::cout << " >> NEWEVAL: " << input << std::endl;
				std::cout << std::endl;
				break;
			case INVALID:
				std::cout << std::endl;
				std::cout << "-- INVALID OPERATION -- " << std::endl;
				std::cout << std::endl;
				break;
			case QUIT:
				std::cout << std::endl;
				std::cout << " QUIT" << std::endl;
				std::cout << std::endl;
				break;
			default:
				std::cout << std::endl;
				std::cout << "** UNDEFINED OPERATION **" << std::endl;
				std::cout << std::endl;
				break;
		}		
	}	
}

void calculate(int operation) {
	
	SymbolicSquareMatrix sm2 = stck.top();
	stck.pop();
	SymbolicSquareMatrix sm1 = stck.top();
	stck.pop();
	SymbolicSquareMatrix res;
	
	switch (operation) {
		case PLUS:
			res = sm1 + sm2;
			break;
		case MINUS:
			res = sm1 - sm2;
			break;
		case MULTIPLY:
			res = sm1 * sm2;
			break;
	}
	
	stck.push(res);
}

void printUserInterface(){	
	
	std::cout << "________________________________________________________________________" << std::endl;
	std::cout << "###################################### | INPUT FORMAT: '[[1,3][c,d]]'  #" << std::endl;
	std::cout << "######## SUURI MATRIISILASKIN ######## | OPERAATIOT: '+', '-', '*', '='#" << std::endl;
	std::cout << "###################################### | NEW EVALUATION: 'x=5'         #" << std::endl;
	std::cout << "-----------------------------------------------------------------------#" << std::endl;		
	std::cout << "Input ('quit' lopettaa):> ";
}


std::string getUserInput() {
	
	std::string str;
	std::cin >> str;
	return str;
}

int processInputType(std::string str) {
	
	std::stringstream input;	
	input << str;
	
	char valEntry = (char) 0;	
	
	if (isspace(input.str().front() || input.str().size() == 0)) {		
		return EMPTY;		
	}
	else if (input.str().front() == '[' && input.peek() == '[') {
		
		try {
			SymbolicSquareMatrix sm(input.str());
			stck.push(sm);
			return MATRIX;
		}
		catch (std::exception& e){
			std::cout << "Input was not valid SquareMatrix.";
			return INVALID;
		}
	}
	else if (input.str().size() == 1 && input.str().front() == '=') {
		
		if (stck.size() >= 1) {
			return EVALUATE;		
		}
		else {
			return INVALID;
		}		
	}
	else if (input.str().size() == 1 && input.str().front() == '+') {
		
		if (stck.size() >= 2) {
			return PLUS;
		}
		else {
			std::cout << "Not enough matrices in stack to perform operation." << std::endl;
			return INVALID;
		}	
	}
	else if (input.str().size() == 1 && input.str().front() == '-') {
		
		if (stck.size() >= 2) {
			return MINUS;
		}
		else {
			std::cout << "Not enough matrices in stack to perform operation." << std::endl;
			return INVALID;
		}
	}
	else if (input.str().size() == 1 && input.str().front() == '*') {
		
		if (stck.size() >= 2) {
			return MULTIPLY;
		}
		else {
			std::cout << "Not enough matrices in stack to perform operation." << std::endl;
			return INVALID;
		}
	}
	else if (isalpha(input.str().front()) && str.at(1) == '=') {
		
		try {
			valEntry = str[0];			
			str.erase(0, 2);			
			int number = std::stoi(str);				
			valu[valEntry] = number;
		}
		catch (std::exception& e) {
			return INVALID;
		}
		
		return NEWEVAL;
	}
	else if (!input.good()) {
		return INVALID;
	}	
	else if (str == "quit" ){
		return QUIT;
	}
	else if (input.good()) {
		return UNDEFINED;
	}
	else {
		std::cout << "Unknown error has occurred, variable log:" << std::endl;
		std::cout << "Input:" << input.str() << std::endl;
		std::cout << "Input str:" << str << std::endl;
		std::cout << "valEntry:" << valEntry << std::endl;		
		
		return INVALID;
	}
		
}



