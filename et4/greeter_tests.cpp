#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "greeter.h"
#include <string>

TEST_CASE("Greeter basic tests I", "[true]"){	
	
	Greeter gr;
	gr.addGreet("Hello!");
	CHECK(gr.sayHello() == "Hello!\n");
	
	gr.addGreet("Moi.");
	gr.addGreet("Terve.");
	gr.addGreet("Iltaa.");
	
	CHECK(gr.sayHello() == "Hello!\nMoi.\nTerve.\nIltaa.\n");
	
	Greeter gr2(gr);
	CHECK(gr2.sayHello() == "Hello!\nMoi.\nTerve.\nIltaa.\n");	
	
}

TEST_CASE("Greeter basic tests II", "[false]"){	
	
	Greeter gr;
	
	gr.addGreet("Morjensta moi!");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi!");
	
	gr.addGreet("Moi.");
	gr.addGreet("Terve.");
	gr.addGreet("Iltaa.");
	
	CHECK_FALSE(gr.sayHello() == "\nMorjensta moi.\nMoi.\nTerve.\nIltaa.\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi\nMoi.\nTerve.\nIltaa.\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.Moi.\nTerve.\nIltaa.\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.\nMoi.\nTerve.\nIltaa.");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.\nMoi.\nTerve.\nIltaa.\n\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.\nMoi.\nTerve.\nIltaa.\n ");
	
	//Greeter gr2(gr&&);
	//CHECK_FALSE(gr2.sayHello() == gr.sayHello());
}

