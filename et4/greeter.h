#ifndef GREETER_H_INCLUDED
#define GREETER_H_INCLUDED
#include <string>
#include <vector>
#include <memory>

class Greeter {
	
	private:
		std::vector<std::unique_ptr<std::string>> greetings;
		
	public:
		Greeter():greetings{}{};
		Greeter(const std::string &g);
		Greeter(const Greeter& other);
		Greeter(Greeter&& other);
		virtual ~Greeter()=default;	
		std::string sayHello() const;
		void addGreet(const std::string&);
		
		Greeter& operator=(Greeter& other);
		Greeter& operator=(Greeter&& other);
};

#endif // GREETER_H_INCLUDED
