#include "greeter.h"
#include <vector>
#include <sstream>
#include <string>

Greeter::Greeter(const std::string &g) {
	greetings.push_back(std::unique_ptr<std::string>(new std::string(g)));
}

Greeter::Greeter(const Greeter& other) {
	
	for (auto& o : other.greetings) {
		greetings.push_back(std::unique_ptr<std::string>(new std::string(*o)));
	}
}

Greeter::Greeter(Greeter&& other) {
	
	*this = std::move(other);
}

std::string Greeter::sayHello() const {
	
	std::stringstream ss;
	
	for (auto& i : greetings) {
		ss << *i << std::endl;
	}
	
	return ss.str();
}

void Greeter::addGreet(const std::string& g) {
	
	greetings.push_back(std::unique_ptr<std::string>(new std::string(g)));
}

Greeter& Greeter::operator=(Greeter& other) {
	
	if(this == &other) return *this;	
	
	greetings.clear();
	
	for (auto& o : other.greetings) {
		greetings.push_back(std::unique_ptr<std::string>(new std::string(*o)));
	}
	
	return *this;
}

Greeter& Greeter::operator=(Greeter&& other) {
	
	if(this == &other) return *this;
	
	// Free existing resource
	greetings.clear();
	
	// Copy data
	greetings = std::move(other.greetings);
	
	return *this;
}
