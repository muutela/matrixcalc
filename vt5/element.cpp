/**
    \file element.cpp
    \brief Element class/interface code
*/
#include "element.h"

#include <ostream>

std::ostream& operator<<(std::ostream& os, const Element& e){
	
	os << e.toString();
	
	return os;
}
