/**
    \file symbolicmatrix.h
    \brief Header file for SymbolicSquareMatrix
*/
#ifndef SYMBOLICMATRIX_H_INCLUDED
#define SYMBOLICMATRIX_H_INCLUDED

#include "intelement.h"
#include "concretematrix.h"

#include <string>
#include <ostream>
#include <memory>
#include <vector>

/**
    \class SymbolicSquareMatrix
    \brief A class for symbolic square matrices
*/
class SymbolicSquareMatrix {
	
	private:
		int n;
		std::vector<std::vector<std::unique_ptr<Element>> > elements{};
	
	public:
		
		/**
        	\brief Default constructor
    	*/
		SymbolicSquareMatrix():n{0}{};
		
		/**
			\brief Parametric constructor
			\param valid string for initializing SymbolicSquareMatrix "[[a,1][c,4]]"
			\throws std::invalid_argument if string is invalid
		*/
		SymbolicSquareMatrix(const std::string&);
		
		/**
        	\brief Copy constructor
    	*/
		SymbolicSquareMatrix(const SymbolicSquareMatrix& m);
		
		/**
        	\brief Move constructor
    	*/
		SymbolicSquareMatrix(SymbolicSquareMatrix&& m);
		
		/**
        	\brief Destructor
    	*/
		~SymbolicSquareMatrix()=default;
		
		/**
        	\brief Transpose SymbolicSquareMatrix
        	\return Transposed SymbolicSquareMatrix
    	*/
		SymbolicSquareMatrix transpose() const;
		
		/**
        	\brief Assignment operator for SymbolicSquareMatrix
        	\param Matrix m to be assigned to object value
        	\return SymbolicSquareMatrix
    	*/
		SymbolicSquareMatrix& operator=(const SymbolicSquareMatrix& m);
		
		/**
        	\brief Move assignment operator for SymbolicSquareMatrix
        	\param Matrix m to be assigned to object value
        	\return SymbolicSquareMatrix
    	*/
		SymbolicSquareMatrix& operator=(SymbolicSquareMatrix&& m);
		
		/**
        	\brief Equals operator for SymbolicSquareMatrix
        	\param Matrix m to be compared to object value
        	\return boolean
    	*/
		bool operator==(const SymbolicSquareMatrix& m) const;
		
		/**
        	\brief Print SymbolicSquareMatrix string representation to ostream
        	\param stream to print into
    	*/
		void print(std::ostream& os) const;
		
		/**
        	\brief Creates a string presentation of the SymbolicSquareMatrix
        	\return string presentation of SymbolicSquareMatrix: [[1,2][3,4]]
    	*/
		std::string toString() const;
		
		/**
        	\brief Creates SymbolicSquareMatrix from a symbolic [[a,b][c,d]] square matrix
        	\param Valuation map<char, int>
        	\return ConcreteSquareMatrix: [[1,2][3,4]]
    	*/
		ConcreteSquareMatrix evaluate(const Valuation& val) const;
		
		/**
        	\brief Check if input string is valid SymbolicSquareMatrix
        	\param String to be checked
    	*/
    	void isSquareMatrix(const std::string &str);	
};

/**
    \brief Output operator
    \param osstream to print in, 
    \param SymbolicSquareMatrix to print from
    \return ostream
*/
std::ostream& operator<<(std::ostream&, const SymbolicSquareMatrix&);

#endif
