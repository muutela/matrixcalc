/**
    \file intelement.cpp
    \brief IntElement class code
*/
#include "intelement.h"
#include "element.h"

#include <sstream>

int IntElement::getVal() const {
	return val;
}

void IntElement::setVal(int v) {
	val = v;
}

IntElement& IntElement::operator+=(const IntElement &i) {
	val += i.val;
	return *this;
}

IntElement& IntElement::operator-=(const IntElement &i) {
	val -= i.val;
	return *this;
}

IntElement& IntElement::operator*=(const IntElement &i) {
	val *= i.val;
	return *this;
}

bool IntElement::operator==(const IntElement& i) const{
	return getVal() == i.getVal();
}

Element* IntElement::clone() const {
		
	return new IntElement(val);
	//return new IntElement(*this);
}

std::string IntElement::toString() const {
	
	std::stringstream ss;
	ss << getVal();
	
	return ss.str();
}

int IntElement::evaluate(const Valuation& v) const {

	return val;
	//return v.at(val);
}

// Basic operators
IntElement operator+(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum += j;
	return sum;
}

IntElement operator-(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum -= j;
	return sum;
}

IntElement operator*(const IntElement& i, const IntElement& j) {
	IntElement sum{i};
	sum *= j;
	return sum;
}

