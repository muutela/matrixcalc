/**
    \file element.h
    \brief Header for Element class
*/
#ifndef ELEMENT_H_INCLUDED
#define ELEMENT_H_INCLUDED

#include "valuation.h"

#include <ostream>
#include <string>

class Element {
	
	public:
		/**
        	\brief Destructor
    	*/
		virtual ~Element() = default;
		
		/**
        	\brief Clone Element, overridden by IntElement, VariableElement
        	\return Pointer to new Element
    	*/
		virtual Element* clone() const = 0;
		
		/**
        	\brief Creates a string presentation of Element, overridden by IntElement, VariableElement
        	\return string representation of Element
    	*/
		virtual std::string toString() const = 0;
		
		/**
        	\brief Retrieves int value from a map<char,int> char symbol, overridden by IntElement, VariableElement
        	\param Valuation map<char, int>
        	\return int value mapped to char
    	*/
		virtual int evaluate(const Valuation& v) const = 0;
};

/**
    \brief Output operator
    \param osstream to print in
    \param Element to print from
    \return ostream
*/
std::ostream& operator<<(std::ostream& os, const Element& e);

#endif
