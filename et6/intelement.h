/**
    \file intelement.h
    \brief Header for IntElement class
*/
#ifndef INTELEMENT_H_INCLUDED
#define INTELEMENT_H_INCLUDED
#include <ostream>
#include "element.h"

/**
    \class IntElement
    \brief A class for IntElement
*/
class IntElement : public Element {
	
	private:
		int val;
	
	public:		
		
		/**
        	\brief Parametric constructor
        	\param v, initialize IntElement val
    	*/
		IntElement(int v):val{v}{};
		
		/**
        	\brief Default constructor
    	*/
		IntElement():IntElement{0}{};
		
		/**
        	\brief Destructor
    	*/
		virtual ~IntElement()=default;
		
		/**
        	\brief Getter of val
        	\return val value as int
    	*/
		int getVal() const;
		
		/**
        	\brief Setter of val
        	\param value to be set as int
    	*/
		void setVal(int v);
		
		/**
        	\brief Operator for adding IntElements
        	\param const IntElement to add
        	\return Addition result as IntElement
    	*/
		IntElement& operator+=(const IntElement& i);
		
		/**
        	\brief Operator for subtracting IntElements
        	\param const IntElement to subtract
        	\return Subtraction result as IntElement
    	*/
		IntElement& operator-=(const IntElement& i);
		
		/**
        	\brief Operator for multiplying IntElements
        	\param const IntElement to multiply
        	\return Multiplication result as IntElement
    	*/
		IntElement& operator*=(const IntElement& i);
		
		/**
        	\brief Operator for comparing IntElements
        	\param const IntElement to compare
        	\return Comparison result as boolean
    	*/
		bool operator==(const IntElement&) const;
		
		/**
        	\brief Clone IntElement, overrides Element.clone()
        	\return Pointer to new Element
    	*/
		Element* clone() const override;		
		
		/**
        	\brief Creates a string presentation of IntElement, overrides Element.toString()
        	\return string representation of IntElement
    	*/
		virtual std::string toString() const override;
		
		/**
        	\brief Returns val, overrides Element.evaluate()
        	\param Valuation map<char, int>
        	\return int
    	*/
		virtual int evaluate(const Valuation& v) const override;	
};

/**
    \brief Operator for IntElement addition
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator+(const IntElement& i, const IntElement& j);

/**
    \brief Operator for IntElement subtraction
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator-(const IntElement& i, const IntElement& j);

/**
    \brief Operator for IntElement multiplication
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator*(const IntElement& i, const IntElement& j);

#endif
