/**
    \file squarematrix_tests.cpp
    \brief Tests for SquareMatrix class
*/
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "squarematrix.h"
#include <sstream>
#include <ostream>
#include <string>

TEST_CASE("Test constructors","[correct format]"){
	
	CHECK_NOTHROW(SquareMatrix("[[1]]"));
	CHECK_NOTHROW(SquareMatrix("[[1,-2][1,4]]"));
	CHECK_NOTHROW(SquareMatrix("[[1,2,3][1,4,-5][2,7,8]]"));	
    CHECK_NOTHROW(SquareMatrix("[[1,-2,3,4][1,4,5,6][2,7,8,9][7,3,-5,8]]"));
	
	SquareMatrix m{"[[5,6][7,9]]"};
	CHECK(m == SquareMatrix("[[5,6][7,9]]"));
	
	SquareMatrix m1(m);
	CHECK(m == m1);
	
	std::string str = m.toString();
	SquareMatrix mv(std::move(m));
	CHECK(str == mv.toString());
}

TEST_CASE("SquareMatrix bad","[ill-formed]"){
	
	CHECK_THROWS(SquareMatrix("[[1][2,3]]"));
	CHECK_THROWS(SquareMatrix("[[1,2][3]]"));
	CHECK_THROWS(SquareMatrix("[[1,2,4][3,3,5]]"));
	CHECK_THROWS(SquareMatrix("[[1,2][4,5]"));
	CHECK_THROWS(SquareMatrix("[1,3][2,3]]"));
	CHECK_THROWS(SquareMatrix("1,3][2,3]]"));
	CHECK_THROWS(SquareMatrix("[[1,3][2,3"));
	CHECK_THROWS(SquareMatrix("[[1 3][2,3]]"));
	CHECK_THROWS(SquareMatrix("[[1,-33][2,3,5]]"));
	CHECK_THROWS(SquareMatrix("[[1,-33][0,3]]-"));
	CHECK_THROWS(SquareMatrix("[[1,-33][0,3]] "));
		
	SquareMatrix sm1("[[1,2][2,3]]");
	SquareMatrix sm2("[[1,2,3][3,4,6][3,4,6]]");
	CHECK_THROWS(sm1 += sm2);
	CHECK_THROWS(sm1 -= sm2);
	CHECK_THROWS(sm1 *= sm2);
}

TEST_CASE("SquareMatrix toString & print test I", "[true]") {
	
	SquareMatrix sm1("[[1,-2][1,4]]");
	CHECK(sm1.toString() == "[[1,-2][1,4]]");
	
	std::stringstream ss;
	sm1.print(ss);
	CHECK(ss.str() == sm1.toString());
}

TEST_CASE("SquareMatrix toString test II", "[false]") {
	
	SquareMatrix sm1("[[1,-2][1,4]]");
	CHECK_FALSE(sm1.toString() == "[[1,-2][1,4]] ");
}

TEST_CASE("SquareMatrix operators I", "[true]") {
	
	SquareMatrix sm0("[[0,0][0,0]]");
	SquareMatrix sm1("[[1,-2][1,4]]");
	SquareMatrix sm2("[[1,-2][1,4]]");
	
	sm0 -= sm1;
	CHECK(sm1.toString() == "[[1,-2][1,4]]");
	
	sm1 += sm2;	
	CHECK(sm1.toString() == "[[2,-4][2,8]]");
	
	sm0 = sm2;
	CHECK(sm0.toString() == sm2.toString());
	CHECK(sm0 == sm2);
	
	sm0 = sm1 + sm2;
	CHECK(sm0.toString() == "[[3,-6][3,12]]");
	
	std::stringstream test;	
	test << sm0;	
	CHECK(sm0.toString() == test.str());
	
	SquareMatrix sm3("[[1,2,3][1,4,5][2,7,8]]");
	SquareMatrix sm4("[[1,1,2][2,4,7][3,5,8]]");
	SquareMatrix trn = sm3.transpose();
	CHECK(trn.toString() == sm4.toString());
	
	SquareMatrix sm5("[[2,2][3,3]]");
	SquareMatrix sm6("[[1,2][3,4]]");
	sm5 *= sm6;
	CHECK(sm5.toString() == "[[8,12][12,18]]");
	
	SquareMatrix sm7("[[1,2][3,4]]");
	SquareMatrix sm8("[[5,6][7,8]]");
	sm7 *= sm8;	
	CHECK(sm7.toString() == "[[19,22][43,50]]");
	
	SquareMatrix sm9("[[1,2,3][5,3,4][12,3,-5]]");
	SquareMatrix smx("[[0,2,-3][5,33,4][14,3,-5]]");
	SquareMatrix res = sm9 * smx;
	CHECK(res.toString() == "[[52,77,-10][71,121,-23][-55,108,1]]");
	
	SquareMatrix smxi(res);
	SquareMatrix smxii(res);
	res = smxi - smxii;
	CHECK(res.toString() == "[[0,0,0][0,0,0][0,0,0]]");
	
	SquareMatrix sm_eq(sm9);
	CHECK(sm_eq == sm9);
}

TEST_CASE("SquareMatrix operators II", "[false]") {
	
	SquareMatrix sm0("[[0,0][0,0]]");
	SquareMatrix sm1("[[1,-2][1,4]]");
	SquareMatrix sm2("[[1,-2][1,4]]");
	
	sm0 -= sm1;
	CHECK_FALSE(sm1.toString() == "[[1,-2][-1,-4]]");
	
	sm1 += sm2;	
	CHECK_FALSE(sm1.toString() == "[[0,-4][2,8]]");	
	
	sm0 = sm1;
	CHECK_FALSE(sm0.toString() == sm2.toString());
	
	sm1 *= sm0;
	CHECK_FALSE(sm1.toString() == "[[0,-4][2,8]]");
	
	SquareMatrix sm9("[[1,2,3][5,3,4][12,3,-5]]");
	SquareMatrix smx("[[0,2,-3][5,33,4][14,3,-5]]");
	SquareMatrix res = sm9 + smx;
	CHECK_FALSE(res.toString() == "[[1,4,1][10,36,8][26,6,-10]]");
}

