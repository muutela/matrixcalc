/**
    \file intelement.h
    \brief Header for IntElement class
*/
#ifndef INTELEMENT_H_INCLUDED
#define INTELEMENT_H_INCLUDED
#include <ostream>

/**
    \class IntElement
    \brief A class for IntElement
*/
class IntElement {
	
	private:
		int val;
	
	public:
		
		// Listamuotoinen-alustus
		/**
        	\brief Parametric constructor
        	\param v, initialize IntElement val
    	*/
		IntElement(int v):val{v}{};
		
		/**
        	\brief Default constructor
    	*/
		IntElement():IntElement{0}{};
		
		// Virtual, hajotin dynaaminen, kun luokka jossain vaiheessa peritaan
		/**
        	\brief Destructor
    	*/
		virtual ~IntElement()=default;
		
		// {query} = kysely, ei saa muuttaa muuttujan tilaa, const
		/**
        	\brief Getter of val
        	\return value
    	*/
		int getVal() const;
		/**
        	\brief Setter of val
    	*/
		void setVal(int v);
		
		/**
        	\brief Operator for adding IntElements
        	\param IntElement to add
        	\return Addition result as SquareMatrix
    	*/
		IntElement& operator+=(const IntElement& i);
		/**
        	\brief Operator for subtracting IntElements
        	\param IntElement to subtract
        	\return Subtraction result as IntElement
    	*/
		IntElement& operator-=(const IntElement& i);
		/**
        	\brief Operator for multiplying IntElements
        	\param IntElement to multiply
        	\return Multiplication result as IntElement
    	*/
		IntElement& operator*=(const IntElement& i);
		
		/**
        	\brief Operator for comparing IntElements
        	\param IntElement to compare
        	\return Comparison result as boolean
    	*/
		bool operator==(const IntElement&) const;
		
		/**
        	\brief Clone IntElement
        	\return Pointer to new IntElement
    	*/
		IntElement* clone() const;
};

/**
    \brief Operator for IntElement addition
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator+(const IntElement& i, const IntElement& j);

/**
    \brief Operator for IntElement subtraction
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator-(const IntElement& i, const IntElement& j);

/**
    \brief Operator for IntElement multiplication
    \param first member of IntElement
    \param second member of IntElement
    \return The product of IntElements
*/
IntElement operator*(const IntElement& i, const IntElement& j);

/**
    \brief Output operator
    \param os stream to print in
    \param IntElement ref to a IntElement object
*/
std::ostream& operator<<(std::ostream&, const IntElement&);

#endif
