#ifndef GREETER_H_INCLUDED
#define GREETER_H_INCLUDED

#include "greet.h"

#include <string>
#include <vector>
#include <memory>

class Greeter {
	
	private:		
		std::vector<Greet*> greetings;
		
	public:
		
		Greeter():greetings{}{};
		// Stores g into attribute vector		
		Greeter(Greet *g); 
		virtual ~Greeter()=default;	
		// Pushes g to the back of attribute vector		
		void addGreet(Greet *g);
		// Concatenates the greets in the vector separated by new line
		std::string sayHello() const;
		
		/*
		// unique_ptr version
		std::vector<std::unique_ptr<Greet>> greetings;
		
		Greeter(std::unique_ptr<Greet> g); 
		void addGreet(std::unique_ptr<Greet> g);
		
		// old definitions
		Greeter():greetings{}{};
		Greeter(const Greeter& other);
		Greeter(Greeter&& other);	
		
		Greeter& operator=(Greeter& other);
		Greeter& operator=(Greeter&& other);
		*/
};

#endif // GREETER_H_INCLUDED
