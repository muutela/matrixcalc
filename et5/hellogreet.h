#ifndef HELLOGREET_H_INCLUDED
#define HELLOGREET_H_INCLUDED

#include "greet.h"

#include <string>
class HelloGreet : public Greet {
	
	public:
		//HelloGreet();		
		virtual std::string greet() const override;
};

#endif // HELLOGREET_H_INCLUDED
