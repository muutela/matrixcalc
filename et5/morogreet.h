#ifndef MOROGREET_H_INCLUDED
#define MOROGREET_H_INCLUDED

#include "greet.h"

#include <string>
class MoroGreet : public Greet {
	
	public:
		//MoroGreet();
		virtual std::string greet() const override;
};

#endif
