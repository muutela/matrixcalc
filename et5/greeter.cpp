#include "greeter.h"
#include "greet.h"

#include <vector>
#include <sstream>
#include <string>

Greeter::Greeter(Greet *g) {
	greetings.push_back(std::move(g));
}

std::string Greeter::sayHello() const {
	
	std::stringstream ss;
	
	for (auto& i : greetings) {
		ss << i->greet() << std::endl;
	}
	
	return ss.str();
}

void Greeter::addGreet(Greet *g) {
	
	greetings.push_back(std::move(g));
}

