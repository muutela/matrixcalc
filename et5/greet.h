#ifndef GREET_H_INCLUDED
#define GREET_H_INCLUDED

#include <string>
class Greet {
	
	public:
		virtual ~Greet();
		virtual std::string greet() const = 0;
};

#endif // GREET_H_INCLUDED
