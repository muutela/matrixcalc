#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "greeter.h"
#include "greet.h"
#include "hellogreet.h"
#include "morogreet.h"

#include <string>
#include <memory>

TEST_CASE("Greeter basic tests I", "[true]") {
	
	Greeter gr;	
	HelloGreet hg;
	MoroGreet mg;
	
	gr.addGreet(&hg);
	CHECK(gr.sayHello() == "Hello!\n");
	
	gr.addGreet(&mg);
	CHECK(gr.sayHello() == "Hello!\nMoro!\n");
	
	gr.addGreet(&mg);
	CHECK(gr.sayHello() == "Hello!\nMoro!\nMoro!\n");
}

TEST_CASE("Greeter basic tests II", "[false]") {
	
	Greeter gr;	
	HelloGreet hg;
	MoroGreet mg;
	
	gr.addGreet(&hg);
	CHECK_FALSE(gr.sayHello() == "Hello!");
	
	gr.addGreet(&mg);
	CHECK_FALSE(gr.sayHello() == "Hello!Moro!\n");
}

/*
// unique_ptr version
TEST_CASE("Greeter basic tests I", "[true]") {
	
	Greeter gr;	
	
	gr.addGreet(std::unique_ptr<Greet>(new HelloGreet()));
	CHECK(gr.sayHello() == "Hello!\n");
	
	gr.addGreet(std::unique_ptr<Greet>(new MoroGreet()));
	CHECK(gr.sayHello() == "Hello!\nMoro!\n");
	
	gr.addGreet(std::unique_ptr<Greet>(new MoroGreet()));
	CHECK(gr.sayHello() == "Hello!\nMoro!\nMoro!\n");
}

TEST_CASE("Greeter basic tests II", "[false]") {
	
	Greeter gr;	
	
	gr.addGreet(std::unique_ptr<Greet>(new HelloGreet()));
	CHECK_FALSE(gr.sayHello() == "Hello!");
	
	gr.addGreet(std::unique_ptr<Greet>(new MoroGreet()));
	CHECK_FALSE(gr.sayHello() == "Hello!Moro!\n");
}

// old tests
TEST_CASE("Greeter basic tests I", "[true]"){	
	
	Greeter gr;
	gr.addGreet("Hello!");
	CHECK(gr.sayHello() == "Hello!\n");
	
	gr.addGreet("Moi.");
	gr.addGreet("Terve.");
	gr.addGreet("Iltaa.");
	
	CHECK(gr.sayHello() == "Hello!\nMoi.\nTerve.\nIltaa.\n");
	
	Greeter gr2(gr);
	CHECK(gr2.sayHello() == "Hello!\nMoi.\nTerve.\nIltaa.\n");	
	
}

TEST_CASE("Greeter basic tests II", "[false]"){	
	
	Greeter gr;
	
	gr.addGreet("Morjensta moi!");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi!");
	
	gr.addGreet("Moi.");
	gr.addGreet("Terve.");
	gr.addGreet("Iltaa.");
	
	CHECK_FALSE(gr.sayHello() == "\nMorjensta moi.\nMoi.\nTerve.\nIltaa.\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi\nMoi.\nTerve.\nIltaa.\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.Moi.\nTerve.\nIltaa.\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.\nMoi.\nTerve.\nIltaa.");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.\nMoi.\nTerve.\nIltaa.\n\n");
	CHECK_FALSE(gr.sayHello() == "Morjensta moi.\nMoi.\nTerve.\nIltaa.\n ");
	
	//Greeter gr2(gr&&);
	//CHECK_FALSE(gr2.sayHello() == gr.sayHello());
}
*/

