/**
    \file squarematrix.h
    \brief Header for SquareMatrix class
*/
#ifndef SQUAREMATRIX_H_INCLUDED
#define SQUAREMATRIX_H_INCLUDED
#include "intelement.h"
#include <string>
#include <vector>

/**
    \class SquareMatrix
    \brief A class for square matrices
*/
class SquareMatrix {
	
	private:		
		int n;
		std::vector<std::vector<IntElement>> elements;
		
	public:
		
		/**
        	\brief Default constructor
    	*/
		SquareMatrix():n{0}{};
		
		/**
			\brief Parametric constructor
			\param s string for initializing
			\throws std::invalid_argument if string is invalid
		*/		
		SquareMatrix(const std::string& s);		
	
		/**
        	\brief Copy constructor
    	*/
		SquareMatrix(const SquareMatrix&)=default;
		
		/**
        	\brief Destructor
    	*/
		virtual ~SquareMatrix() = default;
		
    	/**
        	\brief Transpose SquareMatrix
        	\param Matrix to be transposed
        	\return Transposed SquareMatrix
    	*/
    	SquareMatrix transpose() const;
    	
    	/**
        	\brief Check if input string is valid SquareMatrix
        	\param String to be checked
    	*/
    	void isSquareMatrix(const std::string &str);
    	
		/**
        	\brief Operator for adding SquareMatrices
        	\param SquareMatrix m to be added
        	\throws std::domain_error if matrices sizes do not match
        	\return Addition result as SquareMatrix
    	*/
		SquareMatrix& operator +=(const SquareMatrix& m);
		
		/**
        	\brief Operator for subtracting SquareMatrices
        	\param SquareMatrix m to subtract
        	\throws std::domain_error if matrices sizes do not match
        	\return Subtraction result as SquareMatrix
    	*/
		SquareMatrix& operator -=(const SquareMatrix& m);
		
		/**
        	\brief Operator for multiplying SquareMatrices
        	\param SquareMatrix m to multiply
        	\throws std::domain_error if matrices sizes do not match
        	\return Multiplication result as SquareMatrix
    	*/
		SquareMatrix& operator *=(const SquareMatrix& m);
				
    	/**
        	\brief Assignment operator for SquareMatrix
        	\param Matrix m to be assigned to object value
        	\return SquareMatrix
    	*/
    	SquareMatrix& operator=(const SquareMatrix& m);
    	
    	/**
        	\brief Equals operator for SquareMatrix
        	\param Matrix m to be compared to object value
        	\return boolean
    	*/
    	bool operator==(const SquareMatrix& m) const;   	
    	
    	/**
        	\brief Print SquareMatrix string representation to ostream
        	\param stream to print into
    	*/
		void print(std::ostream&) const;
		
		/**
        	\brief Creates a string presentation of the SquareMatrix 
        	\return string presentation of SquareMatrix: [[1,2][3,4]]
    	*/
		std::string toString() const;
};

/**
    \brief Operator for SquareMatrix addition
    \param first member of SquareMatrix
    \param second member of SquareMatrix
    \return The product of SquareMatrices
*/
SquareMatrix operator+(const SquareMatrix&, const SquareMatrix&);

/**
    \brief Operator for SquareMatrix subtraction
    \param first member of SquareMatrix
    \param second member of SquareMatrix
    \return The product of SquareMatrices
*/
SquareMatrix operator-(const SquareMatrix&, const SquareMatrix&);

/**
    \brief Operator for SquareMatrix multiplication
    \param first member of SquareMatrix
    \param second member of SquareMatrix
    \return The product of SquareMatrices
*/
SquareMatrix operator*(const SquareMatrix&, const SquareMatrix&);

/**
    \brief Output operator
    \param os stream to print in
    \param SquareMatrix ref to a SquareMatrix object
*/
std::ostream& operator<<(std::ostream&, const SquareMatrix&);

#endif // SQUAREMATRIX_H_INCLUDED
