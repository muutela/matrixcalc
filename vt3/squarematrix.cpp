/**
    \file squarematrix.cpp
    \brief SquareMatrix class code
*/
#include "squarematrix.h"
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <string>
#include <numeric>

SquareMatrix::SquareMatrix(const std::string& s) {
	
	isSquareMatrix(s);	
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& m) {	
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	auto m_row = m.elements.begin();
	
	for (auto& row : elements) {
		std::transform(row.begin(), row.end(),
			m_row->begin(), row.begin(), std::plus<IntElement>());
		m_row++;
	}
	
	return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& m) {
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}

	auto m_row = m.elements.begin();
	
	for (auto& row : elements) {
		std::transform(row.begin(), row.end(),
			m_row->begin(), row.begin(), std::minus<IntElement>());
		m_row++;
	}
	
	return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& m) {
	
	if (n != m.n) {
		throw std::domain_error("Matrix dimensions do not match.");
	}
	
	SquareMatrix rightMat = m.transpose();
	std::vector<std::vector<IntElement>> newElements{};
	
	for (auto& lrow : elements) {
		std::vector<IntElement> nextRow{};
		
		for(auto& row: rightMat.elements) {
			std::vector<IntElement> prodRow = lrow;
			std::transform(row.begin(), row.end(), lrow.begin(), prodRow.begin(),
				std::multiplies<IntElement>());
			IntElement sum = std::accumulate(prodRow.begin(), prodRow.end(),
				IntElement{0}, std::plus<IntElement>());
			nextRow.push_back(sum);
		}
		newElements.push_back(nextRow);
	}
	
	elements = newElements;
	
	return *this;
}

SquareMatrix& SquareMatrix::operator=(const SquareMatrix& m){
	
	if (this == &m) return *this;
	
	n = m.n;	 
	elements = m.elements;
	
	return *this;
}

bool SquareMatrix::operator==(const SquareMatrix& m) const {
	
	return elements == m.elements;
}

SquareMatrix operator+(const SquareMatrix& m1, const SquareMatrix& m2) {	
	
	SquareMatrix sm(m1);
	sm += m2;
	
	return sm;
}

SquareMatrix operator-(const SquareMatrix& m1, const SquareMatrix& m2) {
	
	SquareMatrix sm(m1);
	sm -= m2;
	
	return sm;
}

SquareMatrix operator*(const SquareMatrix& m1, const SquareMatrix& m2) {
	
	SquareMatrix sm(m1);	
	sm *= m2;
	
	return sm;
}

std::ostream& operator<<(std::ostream& os, const SquareMatrix& m) {
	
	os << m.toString();

	return os;
}

SquareMatrix SquareMatrix::transpose() const {

	SquareMatrix sm;
	
	std::vector<std::vector<IntElement>> new_elts;
	
	// Empty rows
	for (int i=0; i < n; i++) {
		new_elts.push_back(std::vector<IntElement>());
	}
	
	// Pystyriveiksi tulee matriisin vaakarivit
	// [ 
	//   [1,2,3]    [1,1,2] 
	//   [1,4,5] -> [2,4,7]
	//   [2,7,8]    [3,5,8]
	
	//   [1,2,3][1,4,5][2,7,8]
	// 	 [1,1,2][2,4,7][3,5,8]
	
	
	for (auto& row : elements) {
		int i = 0;
		for (auto& elem : row) {
			new_elts[i].push_back(elem);
			i++;
		}		
	}
	
	sm.elements = new_elts;
	sm.n = n;
	return sm;
}

// outputs the matrix into the stream os in the form [[1,2][3,4]]
void SquareMatrix::print(std::ostream& os) const {
	
	os << toString();
}

// returns string representation of the matrix in the form [[1,2][3,4]]
std::string SquareMatrix::toString() const {
	
	// VT3
	std::stringstream ss;
	
	ss << "[";
	
	for(auto& row:elements) {
		ss << "[";
		
		bool firstRound = true;
		for(auto& elem:row) {
			if (!firstRound) {
				ss << ",";
			}
			ss << elem;
			firstRound = false;
		}
		ss << "]";
	}
	ss << "]";
	
	return ss.str();
}

void SquareMatrix::isSquareMatrix(const std::string &str) {
	char c;	
	int number;
	int matrix_columns = 0;
	int matrix_rows = 0;
	int matrix_expected_size = 0;
	
	std::stringstream input(str);
	
	input >> c;
	
	// Examine string input to verify correct square-matrix format
	// Example: ->[<-[1,2,3][4,5,6]] can not be anything else than [	
	if (!input.good() || c != '[')
		throw std::invalid_argument("String is not a square matrix.");
	
	input >> c;
	
	// While char is not ] input-reference: [[1,2,3][4,5,6]->]<-	
	while (c != ']') {
		
		// Return false if char not [ input-reference: [->[<-1,2,3][4,5,6]]
		if (!input.good() || c != '[')
			throw std::invalid_argument("String is not a square matrix.");
		
		std::vector<IntElement> row;
		matrix_columns = 0;
		
		do {
			
			input >> number;
			
			// Return false if input not integer, [[->1<-, 2,3]
			if (!input.good())
				throw std::invalid_argument("String is not a square matrix.");
			
			row.push_back(IntElement{number});
			matrix_columns++;
			
			input >> c;
			
			// Return false if input not , OR ]
			if (!input.good() || (c != ',' && c != ']'))
				throw std::invalid_argument("String is not a square matrix.");
						
		} while (c != ']');
		
		elements.push_back(row);
		
		// Get square matrix dimensions
		if (matrix_rows == 0)
			matrix_expected_size = matrix_columns;
			
		matrix_rows++;
		
		// Check square-matrix colums & rows are equal
		if (matrix_expected_size != matrix_columns)
			throw std::invalid_argument("String is not a square matrix.");
				
		input >> c;
		
		// Return false if input not [ or ]
		if (!input.good() || (c != '[' && c != ']'))
			throw std::invalid_argument("String is not a square matrix.");	
	}
	
	// Check square-matrix dimensions are correct
	if (matrix_expected_size != matrix_rows)
		throw std::invalid_argument("String is not a square matrix.");
	
	// Check input ends, getline to get whitespaces
	std::string tail;
	std::getline(input, tail);
	
	if (tail != "" || isspace(tail.front())) {
		throw std::invalid_argument("String is not a square matrix.");		
	}
	
	n = matrix_expected_size;
}
