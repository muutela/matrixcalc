/**
    \file intelement_tests.cpp
    \brief Tests for IntElement class
*/
#include "catch.hpp"
#include "intelement.h"

TEST_CASE("IntElement basic tests", "[first]"){
	
	IntElement zero;	
	CHECK(zero.getVal()==0);
	
	zero.setVal(-5);	
	CHECK(zero.getVal()==-5);
}

TEST_CASE("IntElement operators test", "[second]"){
	
	IntElement e10{10};
	IntElement e25{25};
	IntElement res{0};
	res = e10 + e25; // elem.operator+=(zero); /sama asia
	CHECK(res.getVal()==35);
	
	res.setVal(0);	
	res = e10 - e25; // res = operator+(e10, e25);
	CHECK(res.getVal()==-15);
	
	IntElement e5{5};
	IntElement e6{6};
	e5 *= e6;
	CHECK(e5.getVal() == 30); // e5(5) * e6(6)
}
